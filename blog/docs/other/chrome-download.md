# chrome离线安装包下载

最新稳定版：
[https://www.google.com/intl/zh-CN/chrome/browser/?standalone=1](https://www.google.com/intl/zh-CN/chrome/browser/?standalone=1)

最新测试版：
[https://www.google.com/intl/zh-CN/chrome/browser/?standalone=1&extra=betachannel](https://www.google.com/intl/zh-CN/chrome/browser/?standalone=1&extra=betachannel)

最新开发版：
[https://www.google.com/intl/zh-CN/chrome/browser/?standalone=1&extra=devchannel](https://www.google.com/intl/zh-CN/chrome/browser/?standalone=1&extra=devchannel)


另外,在ubuntu下可以安装chromium
```bash
sudo add-apt-repository ppa:chromium-daily
sudo apt-get update
sudo apt-get install chromium-browser
```
