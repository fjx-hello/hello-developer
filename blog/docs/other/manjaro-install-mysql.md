# manjaro安装MySQL

```bash
# 安装
➜  ~ sudo pacman -S mysql


# 初始化MySQL
# root@localhost: fip6sD,Et)_&，会输出root初始密码
➜  ~ sudo mysqld --initialize --user=mysql --basedir=/usr --datadir=/var/lib/mysql
2019-10-28T17:26:22.653266Z 0 [Warning] [MY-010915] [Server] 'NO_ZERO_DATE', 'NO_ZERO_IN_DATE' and 'ERROR_FOR_DIVISION_BY_ZERO' sql modes should be used with strict mode. They will be merged with strict mode in a future release.
2019-10-28T17:26:22.653314Z 0 [System] [MY-013169] [Server] /usr/bin/mysqld (mysqld 8.0.17) initializing of server in progress as process 8610
2019-10-28T17:26:25.513724Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: fip6sD,Et)_&
2019-10-28T17:26:27.125793Z 0 [System] [MY-013170] [Server] /usr/bin/mysqld (mysqld 8.0.17) initializing of server has completed

# 设置开启
➜  ~ sudo systemctl enable mysqld.service
➜  ~ sudo systemctl daemon-reload
# 启动MySQL
➜  ~ sudo systemctl start mysqld.service


# 初始化安全设置，按照提示一步步设置就好
~ mysql_secure_installation

Securing the MySQL server deployment.

Enter password for user root:

The existing password for the user account root has expired. Please set a new password.

New password:  #设置新的密码

Re-enter new password:

VALIDATE PASSWORD COMPONENT can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD component?

Press y|Y for Yes, any other key for No: y

There are three levels of password validation policy:

LOW    Length >= 8
MEDIUM Length >= 8, numeric, mixed case, and special characters
STRONG Length >= 8, numeric, mixed case, special characters and dictionary                  file

Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 0 # 设置密码安全级别
Using existing password for root.

Estimated strength of the password: 100
Change the password for root ? ((Press y|Y for Yes, any other key for No) :

 ... skipping.
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y # 是否删除匿名用户
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : # 是否禁止root用户远程访问

 ... skipping.
By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y # 是否删除测试库
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y # 是否刷新数据库授权
Success.

All done!


```