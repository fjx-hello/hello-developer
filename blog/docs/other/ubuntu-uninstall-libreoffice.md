# ubuntu下彻底卸载LibreOffice方法

sudo apt-get purge libreoffice?

或

sudo aptitude purge libreoffice?

不要漏掉通配符“?”，否则无法清除/卸载全部 LibreOffice 软件包

或者

sudo apt-get remove --purge libreoffice*
