# 线程池

## jdk提供的常用线程池

使用Executors静态方法可以创建5种不同配置和类型的线程池

::: warning
在高并发场景下[不要使用Executors创建线程池](/java/thread/awesome.html#谨慎使用executors创建线程池)，需要根据实际场景定制参数。
:::


- newCachedThreadPool() 用来创建可缓存的线程，当没有缓存线程可用时，就创建新的工作线程；如果线程闲置的时间超过60秒，将会被移出缓存，内部使用SynchronousQueue作为工作队列。一般用来处理大量短时间任务。
    ```java
    public static ExecutorService newCachedThreadPool(ThreadFactory threadFactory) {
        return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                      60L, TimeUnit.SECONDS,
                                      new SynchronousQueue<Runnable>(),
                                      threadFactory);
    }
    ```

- newFixedThreadPool(int nThreads) 创建固定大小为nThreads的线程池，当活动任务数量超过nThreads，新的任务将在工作队列中等待。具有固定的线程开销，当任务过多时任务需要等待，任务数少时则会有线程浪费。
    ```java
    public static ExecutorService newFixedThreadPool(int nThreads) {
        return new ThreadPoolExecutor(nThreads, nThreads,
                                      0L, TimeUnit.MILLISECONDS,
                                      new LinkedBlockingQueue<Runnable>());
    }
    ```

- newSingleThreadExecutor() 永远只有1个工作线程，保证所有任务按顺序执行。使用FinalizableDelegatedExecutorService包装，屏蔽了很多方法，限制了线程池实例无法修改，例如改变线程池数量。
    ```java
    public static ExecutorService newSingleThreadExecutor() {
        return new FinalizableDelegatedExecutorService
            (new ThreadPoolExecutor(1, 1,
                                    0L, TimeUnit.MILLISECONDS,
                                    new LinkedBlockingQueue<Runnable>()));
    }
    ```

- newSingleThreadScheduledExecutor / newScheduledThreadPool(int corePoolSize) 返回ScheduledExecutorService对象，用于执行定时或者周期性任务，区别在与工作线程是1个还是多个。
    ```java
    public static ScheduledExecutorService newSingleThreadScheduledExecutor() {
        return new DelegatedScheduledExecutorService
            (new ScheduledThreadPoolExecutor(1));
    }
  
    public static ScheduledExecutorService newScheduledThreadPool(int corePoolSize) {
        return new ScheduledThreadPoolExecutor(corePoolSize);
    }
    ```

- newWorkStealingPool(int parallelism) jdk8加入的api，返回ForkJoinPool对象。
    ```java
    public static ExecutorService newWorkStealingPool(int parallelism) {
        return new ForkJoinPool
            (parallelism,
             ForkJoinPool.defaultForkJoinWorkerThreadFactory,
             null, true);
    }
    ```
    


