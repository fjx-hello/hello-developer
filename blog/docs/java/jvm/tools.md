# jvm监控工具

- [perfma](https://opts.console.perfma.com/) 国内jvm在线分析平台
- [GCViewer](https://sourceforge.net/projects/gcviewer/)  图形化gc日志分析工具
- [GCeasy](https://www.gceasy.io/index.jsp) 在线gc日志分析工具

### jstat

jstat -gcutil <pid> <interval>

```bash
# 15200是进程号，1s表示每隔1秒输出一次
jstat -gcutil 15200 1s
S0     S1     E      O      M     CCS    YGC     YGCT    FGC    FGCT     GCT   
0.00  66.05  28.80  93.85  24.03  93.78  45401 1731.240    12   22.165 1753.405
0.00  66.05  51.80  93.85  24.03  93.78  45401 1731.240    12   22.165 1753.405
0.00  66.05  62.20  93.85  24.03  93.78  45401 1731.240    12   22.165 1753.405
```






