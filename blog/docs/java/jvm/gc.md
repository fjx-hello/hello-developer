# 垃圾收集（GC）

## Reference
- 《深入理解Java虚拟机（第2版）》
- [深入理解Java虚拟机（第2版）](https://book.douban.com/subject/24722612/) 第3章
- [这次，真正学懂 Java 垃圾回收机制](https://mp.weixin.qq.com/s/wn_7qve6OCD0SpYILvcKtg)
- [不用找了，深入理解G1垃圾收集器和GC日志，都整理好了](https://mp.weixin.qq.com/s/8NkjFJ_Uy0CStZNKxZUR2g)
- [46张PPT讲述JVM、GC算法和性能调优](https://mp.weixin.qq.com/s?__biz=MzA3MTUzOTcxOQ==&mid=2452965917&idx=1&sn=e9352b6b722fbe80b424bbcf715f8379&scene=21#wechat_redirect)

## 垃圾回收区域

内存垃圾回收的目的是未必避免程序在运行过程中出现内存溢出和内存泄漏问题。

JVM内存划分中，程序计数器、本地方法栈、虚拟机栈所需要的内存大小在代码结构确定后就已知的，并且会随着线程的生命周期创建和销毁，所以这部分内存分配具有确定性，不需要过多关注。

而对于堆和方法区，需要多大内存是要在运行时才能知道的，因为方法的参数不同，调用次数不同，这些都是在运行期动态生成的，所以垃圾回收的职责就是对这部分垃圾对象进行回收，释放内存。

> 回收方法区：java虚拟机规范中，不要求方法区实现垃圾收集器，主要是对方法区进行垃圾收集的性价比不高，但是方法区也是可以在运行时产生新的字面量和类的。例如new String("abc")的时候，如果常量池中不存在“abc”这个对象，则会在常量池中创建。JSP的实现过程是将新的servlet类加载到jvm中。
> 
> 所以方法区（HotSpot用永久代实现方法区）的垃圾回收主要包括：废弃常量和无用类。
> 是否对类进行回收HotSpott提供了-Xnoclassgc来控制

*方法区内存的回收查看《深入理解Java虚拟机（第2版）》3.2.5章节*

## 判断哪些对象可以回收

- 引用计数法：为每个对象创建一个引用计数器，统计所有指向该对象的引用个数，当一个对象引用个数为0时，则说明对象没有引用可以回收
    > 缺点：引用计数法无法处理循环引用的对象

- 可达性分析法：通过“GC Roots”对象集往下搜索引用链，当一个引用链无法到达“GC Roots”时，则说明该引用链的所有对象可以回收（obj5、obj6、obj7无法到达GC Roots，所以可以被回收）。

![可达性分析法](/img/java/jvm/gc-roots.png)

> 引用计数法是目前主流的垃圾标记算法，GC Roots包括
> - 虚拟机栈中引用的对象
> - 方法区中静态属性引用的对象
> - 方法区中常量引用的对象
> - 本地方法（JNI）栈中引用的对象

## 垃圾回收算法

- 标记-清除算法（Mark-Sweep）：最基本的垃圾收集算法，逻辑简单，缺点：效率偏低，容易产生内存碎片

    ![gc-mark-sweep](http://blog.qiniu.fengjx.com//java/jvm/gc-mark-sweep.png)

- 复制算法（Copying）：逻辑简单、效率高、没有内存碎片，但是内存使用率不高，适合存活对象少（需要复制的对象少）的情况
  
    ![gc-copying](http://blog.qiniu.fengjx.com//java/jvm/gc-copying.png)
  
- 标记-整理算法（Mark-Compact）：内存利用率高，没有内存碎片，适合存活对象多，垃圾少的情况（需要移动位置的对象少）

    ![gc-compact](http://blog.qiniu.fengjx.com//java/jvm/gc-compact.png)

## 分代垃圾回收

以上垃圾回收算法各自都有优缺点，为了针对不同场景，jvm开发者对内存做了划分，针对不同区域使用不同的回收算法。

- 新生代
    * 大部分对象在gc时被回收，存活对象少，使用复制算法。为了解决内存占用过高问题，将新生代划分成了Eden, Survivors(s0, s1或者from, to)，内存分配比例是Eden:from:to = 8:1:1
- 老年代
    * 经过多次gc后依然存活的对象进入老年代，所以gc时存活对象多，使用“标记-清除”或者“标记-整理”算法

*详细查看《深入理解Java虚拟机（第2版）》3.3章节*

## 垃圾收集过程

1. 新对象在Eden区创建，当Eden区快满的时候，进行Minor GC(YGC)，将Eden清空，把存活对象放到From区，然后新对象继续在Eden区创建。
2. 当Eden区又满的时候，进行Minor GC(YGC)，清理Eden区和From区垃圾对象，把存活对象放到To区，依次类推，From区和To区交替使用。
3. 当对象经过多次Minor GC(YGC)后依然存活（超过回收年龄，不同垃圾收集器的阈值不一样，可以通过参数修改），进入老年代（Old区）。
4. 如果存活对象太多，From区或者To区放不下，则直接进入Old区。
5. Old区快满时（当内存使用比例达到一定阈值，不同收集器阈值不一样，cms收集器默认是92%），触发Major GC(FGC)，同时对新生代和老年代进行垃圾回收。



## 垃圾收集器

各垃圾收集器类型和适用分代范围

![JVM结构体系](/img/java/jvm/garbage-collector.png)

### Serial

单线程串行收集器，用于年轻代


### CMS
- 初始标记： 标记GC Roots能直接关联到的对象，速度很快；
- 并发标记： 进行GC Roots Tracing的过程；
- 重新标记： 修正并发标记期间因用户程序继续运作而导致标记产生变动的那一部分对象的标记记录，这个阶段的停顿时间一般会比初始标记阶段稍长一些，但比并发标记时间短；
- 并发清除： 整个过程中耗时最长的并发标记和并发清除过程收集器线程都可以与用户线程一起工作，所以，从总体上来说，CMS收集器的内存回收过程是与用户线程一起并发执行的。
    * 优点：并发收集、低停顿
    * 缺点：对CPU资源非常敏感、无法处理浮动垃圾、产生大量空间碎片。

1. 初始标记 (Stop the World事件 CPU停顿， 很短) 初始标记仅标记一下GC Roots能直接关联到的对象，速度很快；

2. 并发标记 (收集垃圾跟用户线程一起执行) 初始标记和重新标记任然需要“stop the world”，并发标记过程就是进行GC Roots Tracing的过程；

3. 重新标记 (Stop the World事件 CPU停顿，比初始标记稍微长，远比并发标记短)修正并发标记期间因用户程序继续运作而导致标记产生变动的那一部分对象的标记记录，这个阶段的停顿时间一般会比初始标记阶段稍长一些，但远比并发标记时间短

4. 并发清理 -清除算法；



**gc日志**
- 初始标记（Inital Mark）会stop-the-world
- 并发标记（Concurrent Mark）
- 最终标记（Final Mark）会stop-the-world)
- 筛选回收。会stop-the-world

1、初始标记(stop the world事件 CPU停顿只处理垃圾)；

2、并发标记(与用户线程并发执行)；

3、最终标记(stop the world事件 ,CPU停顿处理垃圾)；

4、筛选回收(stop the world事件 根据用户期望的GC停顿时间回收)(注意：CMS 在这一步不需要stop the world)

### G1


垃圾收集器 | 回收算法 | 参数 | 垃圾收集过程 | 说明
---------|----------|----------|----------|--------
 A1 | B1 | C1 | |
 A2 | B2 | C2 | |
 A3 | B3 | C3 | |



**gc日志**
```log
2019-10-26T21:38:01.854+0800: 2791045.469: [GC pause (G1 Evacuation Pause) (young), 0.0286596 secs]
   [Parallel Time: 14.8 ms, GC Workers: 28]
      [GC Worker Start (ms): Min: 2791045469.3, Avg: 2791045473.0, Max: 2791045480.5, Diff: 11.2]
      [Ext Root Scanning (ms): Min: 0.0, Avg: 0.9, Max: 5.9, Diff: 5.9, Sum: 24.7]
      [Update RS (ms): Min: 0.0, Avg: 3.0, Max: 6.0, Diff: 6.0, Sum: 82.9]
         [Processed Buffers: Min: 0, Avg: 15.2, Max: 36, Diff: 36, Sum: 427]
      [Scan RS (ms): Min: 0.1, Avg: 0.4, Max: 3.7, Diff: 3.6, Sum: 12.2]
      [Code Root Scanning (ms): Min: 0.0, Avg: 0.0, Max: 0.0, Diff: 0.0, Sum: 0.1]
      [Object Copy (ms): Min: 0.0, Avg: 3.1, Max: 12.8, Diff: 12.8, Sum: 86.9]
      [Termination (ms): Min: 0.0, Avg: 3.4, Max: 3.9, Diff: 3.9, Sum: 94.0]
         [Termination Attempts: Min: 1, Avg: 10.3, Max: 21, Diff: 20, Sum: 289]
      [GC Worker Other (ms): Min: 0.0, Avg: 0.1, Max: 0.3, Diff: 0.3, Sum: 3.5]
      [GC Worker Total (ms): Min: 3.2, Avg: 10.9, Max: 14.6, Diff: 11.4, Sum: 304.2]
      [GC Worker End (ms): Min: 2791045483.7, Avg: 2791045483.8, Max: 2791045484.0, Diff: 0.3]
   [Code Root Fixup: 0.2 ms]
   [Code Root Purge: 0.0 ms]
   [Clear CT: 1.2 ms]
   [Other: 12.5 ms]
      [Choose CSet: 0.0 ms]
      [Ref Proc: 7.1 ms]
      [Ref Enq: 0.0 ms]
      [Redirty Cards: 1.5 ms]
      [Humongous Register: 0.2 ms]
      [Humongous Reclaim: 0.1 ms]
      [Free CSet: 2.8 ms]
   [Eden: 4896.0M(4896.0M)->0.0B(4892.0M) Survivors: 16.0M->20.0M Heap: 5512.3M(8192.0M)->590.6M(8192.0M)]
 [Times: user=0.31 sys=0.00, real=0.03 secs]
```
- user=0.31
    > 表示GC线程占用310毫秒
- real=0.03
    > 表示Young GC实际占用30毫秒
- Eden: 4896.0M(4896.0M)->0.0B(4892.0M)
    > Eden区内存全部清空，释放了4892M内存（有4M存活对象，会放到Survivors区或者Old区）
- Survivors: 16.0M->20.0M
    > Survivors区对象从16M变成20M（从Eden区转过来的）
- Heap: 5512.3M(8192.0M)->590.6M(8192.0M)]
    > 堆内存从5512.3M降到了590.6M