# GC调优方法及案例分析

## Reference

- [从实际案例聊聊Java应用的GC优化](https://tech.meituan.com/2017/12/29/jvm-optimize.html)
- [大型跨境电商JVM调优经历](https://juejin.im/post/5b091ee35188253892389683)
- [Java Jvm 性能调优，有这个5个攻略](https://mp.weixin.qq.com/s?__biz=MzA3MTUzOTcxOQ==&mid=2452966362&idx=2&sn=91599afe52803c229063bc10a2c92954&chksm=88ede6b2bf9a6fa4c08a5debfe84a7382df253012a8f3b59dfc59a79735604b6124493753ceb&scene=21#wechat_redirect)
- [JVM调优必备，死磕GC日志](https://mp.weixin.qq.com/s?__biz=MzA3MTUzOTcxOQ==&mid=2452967124&idx=1&sn=fe97a4f149bb3aedf78f08af5cc9a736&chksm=88ede3bcbf9a6aaa9388c87cd3288061dcb3a359cbc6794024ed7a2fef8c538471039ce10d36&scene=21#wechat_redirect)

GC调优指标

- 停顿时间：垃圾收集器运行时，程序的暂停（STW: stop the world）时间
- 吞吐量：应用程序耗时 / (应用程序耗时 + GC耗时 = 程序运行总时间)
      
## Minor(ygc)频率过高

### Eden去内存太小，很快被占满

解决办法：增加Eden去内存，但是不能无限制增加Eden去内存大小，可能会增加单次Minor GC时间。
需根据对象存活时间和Minor GC时间间隔而定。Minor GC时间包括2部分：扫描新生代对象时间和复制存活对象时间。增加Eden区大小会增加扫描时间，但是可能回收了更多的对象而减少复制对象的时间，不一定会导致单次Minor GC时间增加。
即：如果堆中存在较多的长期存活的对象，增加年轻代空间，反而会增加Minor  GC的时间。如果堆中的短期对象很多，那么扩容新生代，单次Minor GC时间不会显著增加。因此，单次 Minor GC 时间更多取决于 GC 后存活对象的数量，而非 Eden 区的大小。













