# 方法引用

方法引用可以使用操作符“::”来应用已有方法的处理逻辑。简化代码和复用代码逻辑。

方法引用包括几种方式：
- 构造器引用: Class::new
- 静态方法引用: Class::staticMethod
- 实例方法引用: instance:method
- 类方法引用: Class::method, 当method的第一个参数类型是Class时才能使用

示例
```java
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class MethodRefTest {

    public MethodRefTest() {
    }

    public void hello(String name) {
        System.out.println("hello: " + name);
    }

    public static int len(String str) {
        return str == null ? 0 : str.length();
    }

    public static void main(String[] args) {

        // 实例方法引用
        List<String> list = Arrays.asList("a", "b", "c");
        list.forEach(System.out::println);

        // 构造器引用
        Supplier<MethodRefTest> supplier = MethodRefTest::new;
        MethodRefTest mr = supplier.get();

        // 静态方法引用
        Function<String, Integer> fun = MethodRefTest::len;
        int len = fun.apply("abc");
        System.out.println(len);

        // 实例方法引用
        Consumer<String> hello = mr::hello;
        hello.accept("fjx");

        // 构造器引用, 类方法引用, 实例方法引用
        Stream.generate(MethodRefTest::new).limit(5)
            .map(MethodRefTest::hashCode)
            .forEach(System.out::println);

    }
}
```






