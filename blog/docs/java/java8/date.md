# 新的Date&Time API

java8之前的日期相关对象中存在的问题：
1. 非线程安全，例如：SimpleDateFormat, Calendar, TimeZone
2. 设计不合理：java.util和java.sql的包中都有日期类，并且命名相同。而格式化解析类却定义在java.text包，
3. 不支持时区：Date类本身不支持时区，需要使用java.util.Calendar和java.util.TimeZone来处理

java8日期类在java.time包中，对之前版本做了改进：
1. 解决了线程安全和时区的问题（对象不可变）
2. api优化，使用非常方便，包括日期（LocalDate），时间（LocalTime），日期/时间（LocalDateTime），时间戳（instants），时间间隔（Duration、Period）的操作


## api
java.time
- LocalDate（只包含日期）, LocalTime（只包含时间）, LocalDateTime（包含日期和时间）。LocalTime每月年月日相关api
    1. now() - 获取当前时间
    2. of() - 创建指定日期
    3. plusDays(), plusWeeks(), plusMonths(), plusYears() - 增加天，周，月，年
    4. minusDays(), minusWeeks(), minusMonths(), minusYear() - 减少天，周，月，年
    5. plusHours(), plusMinutes(), plusSeconds() - 增加小时，分钟，秒
    6. minusHours(), minusMinutes(), minusSeconds() - 减少小时，分钟，秒
    7. plus(), minus() - 增加或减少Duration或Period
    8. getYear(), getMonth(), getDayOfMonth(), getHour(), ... - 获取日期的年、月、日、小时，等...
    9.  withYear(), withMonth(), withDayOfMonth(), ... - 修改日期为指定的年、月、日，等
    10. isBefore(), isAfter() - 日期（时间）比较
    11. until() - 获取两个日期（时间）的时间差
    12. isLeapYear() - 是否是闰年（只适用LocalDate）
- Instant - 时间戳
- Duration - 时间间隔
- Period - 日期间隔
- TemporalAdjuster - 时间调节器，用来调整时间
    1. TemporalAdjusters - 有很多静态方法可以创建TemporalAdjuster，例如当前月份的第一天、最后一天等
- ZoneId - 时区信息
- ZoneOffset - 时区偏移量
- ZonedDate, ZonedTime, ZonedDateTime - 带时区的日期（时间）
- Clock - 时钟


java.time.format
- DateTimeFormatter - 时间自定义格式化


## 示例
```java
public static void main(String[] args) {
    // 日期（时间）常用api
    LocalDateTime nowDateTime = LocalDateTime.now();
    System.out.println("当前日期和时间：" + nowDateTime);
    LocalDate nowDate = LocalDate.now();
    System.out.println("当前日期：" + nowDate);
    LocalTime nowTime = LocalTime.now();
    System.out.println("当前时间：" + nowTime + ", 不显示毫秒：" + nowTime.withNano(0));

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    System.out.println("格式化当前日期和时间：" + dtf.format(nowDateTime));

    LocalDateTime date1 = LocalDateTime.parse("2000-01-01 21:24:54", dtf);
    System.out.println("date1: " + date1);

    LocalDateTime date2 = LocalDateTime.of(2000, 07, 28, 12, 30, 10);
    System.out.println("date2: " + date2);

    LocalDateTime date3 = date1.plusDays(30);
    System.out.println("date3: " + date3);

    long day = date1.until(date3, ChronoUnit.DAYS);
    System.out.println("date1与date3相差天数：" + day);
    Period period = Period.between(date1.toLocalDate(), date3.toLocalDate());
    System.out.println(
            String.format("date1与date3相差：%d年 %d月 %d日", period.getYears(), period.getMonths(), period.getDays()));

    LocalDate firstDayOfMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
    LocalDate lastDayOfMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
    System.out.println(String.format("当月的第一天：%s, 最后一天：%s", firstDayOfMonth, lastDayOfMonth));

    // 时区
    Set<String> zoneIds = ZoneId.getAvailableZoneIds();
    // zoneIds.forEach(System.out::println); // 遍历所有时区

    ZoneId sysZone = ZoneId.systemDefault();
    System.out.println("系统时区：" + sysZone);

    ZoneId losZone = ZoneId.of("America/Los_Angeles");
    LocalDateTime losDateTime = LocalDateTime.now(losZone);
    System.out.println("当前洛杉矶时间：" + losDateTime);

    // 时间戳
    long currentTimeMillis = System.currentTimeMillis();
    System.out.println("long转Instant：" + Instant.ofEpochMilli(currentTimeMillis).toEpochMilli());

    Instant instant = Instant.now();
    System.out.println("当前时间戳：" + instant.toEpochMilli());
    Instant instant2 = nowDateTime.atZone(sysZone).toInstant(); // LocalDateTime转Instant

    long duraMillis = Duration.between(instant2, instant).toMillis();
    System.out.println("instant2与instant相差毫秒数：" + duraMillis);

    // 与Instant、Date、LocalDateTime相互转化
    LocalDateTime insToDateTime = LocalDateTime.ofInstant(instant, sysZone);
    System.out.println("Instant转LocalDateTime：" + insToDateTime);

    Date date = Date.from(instant);
    System.out.println("Instant转Date：" + date);

    Instant dateTimeToInstant = nowDateTime.atZone(sysZone).toInstant();
    System.out.println("LocalDateTime转Instant：" + dateTimeToInstant.toEpochMilli());
}
```

