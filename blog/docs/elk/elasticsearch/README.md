# elasticsearch

## elasticsearch与solr对于

- 离线数据搜索，solr比elasticSearch快（大概4倍）
- 实时数据搜索，elasticSearch比solr快，因为solr建立索引时存在IO阻塞问题
- 在数据量增加的时候，elasticSearch搜索速度变化不大，更适合大数据量搜索

## elasticsearch基本概念

### 运维相关

- 集群（cluster）：多个es实例协同组成的集群
- 节点（node）：集群中每个es实例是一个节点
- 主分片（primary shard）：es将数据拆分成多个分片，存储到不同节点上
    * 根据 [hash(id) % 节点数] 路由到具体节点
- 副本（replica shard）：副本是主分片的一份拷贝数据，与主分片分开存储到其他节点上。当集群中的主分片实节点出现故障时，会将其他副本节点切换为主分片节点
    * 副本分片数可以动态调整
    * 增加副本数可以提高服务可用性

### 数据相关

- 索引（index）
- 文档（document）
- 









