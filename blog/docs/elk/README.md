# ELK搭建

## 使用docker模拟集群环境

```bash
docker network create -d bridge elk-net

docker run -it --name elk1 --network elk-net openjdk:11 bash

root@899d5dd71dbd:/app# ifcondif
bash: ifcondif: command not found

apt update 

apt install net-tools vim supervisor

```


## elasticsearch集群搭建

1. 下载
```bash
wget wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.1.1-linux-x86_64.tar.gz
```

## kafka


1. 下载
```bash
wget https://mirrors.tuna.tsinghua.edu.cn/apache/zookeeper/zookeeper-3.4.14/zookeeper-3.4.14.tar.gz

wget http://mirrors.tuna.tsinghua.edu.cn/apache/kafka/2.2.1/kafka_2.12-2.2.1.tgz

```



## filebeat

1. 下载
```bash
wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.1.1-linux-x86_64.tar.gz
```



filebeat-accesslog
```yaml
name: 172.19.0.4
filebeat.shutdown_timeout: 3s

filebeat.inputs:
- input_type: log
  paths:
  - /app/work/resty/logs/hello_nginx_access.log
  fields:
    type: work_access_hello
  tail_files: true
  ignore_older: 12h
  clean_inactive: 24h
  json.keys_under_root: true
  json.add_error_key: true

output.kafka:
  hosts: ["172.19.0.2:9092"]
  topic: '%{[fields.type]}'
  partition.round_robin:
    reachable_only: true
  required_acks: 1
  compression: gzip
  max_message_bytes: 1000000
  worker: 1
```


## logstash

1. 下载
```
wget https://artifacts.elastic.co/downloads/logstash/logstash-7.1.1.tar.gz
```

- logstash-access.conf
```
input {
    kafka {
        bootstrap_servers =>  "172.19.0.2:9092"
        group_id => "logstash_work_access"
        topics_pattern => ["work_access.*"]
        codec    => json
        consumer_threads => 3
    }
}

filter {

    json {
        source => "message"
    }

    if [localtime] {
        date {
            match => [ "localtime", "ISO8601" ]
        }
    }

    mutate {
        rename => { "[beat][name]" => "host" }
        remove_field => [ "beat", "input_type", "source", "localtime"]
    }

    ruby {
        code => "event.set('responsetime', (event.get('responsetime').to_f * 1000).to_i.round(8).to_i)"
    }

}

output {

    elasticsearch {
        hosts => ["172.19.0.2:9200"]
        index => "work_access-%{+YYYY.MM.dd}"
        codec => json
    }

}
```

