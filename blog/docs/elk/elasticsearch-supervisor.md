# 使用supervisor配置elasticsearch-7.2本地伪集群

> supervisor是一个强大方便的进程管理工具，安装和使用方法自行google
> 官网：<http://supervisord.org/>

1. 下载elasticsearch
```bash
# 下载
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.2.0-linux-x86_64.tar.gz

# 解压
tar -zxvf elasticsearch-7.2.0-linux-x86_64.tar.gz
```
2. 安装中文分词插件
    ```
    bin ./elasticsearch-plugin install analysis-icu
    ```

3. 修改配置文件
```
# config/elasticsearch.yml，节点名和ip自行修改
network.host: 192.168.1.106
discovery.seed_hosts: ["192.168.1.106"]
cluster.initial_master_nodes: ["node-00", "node-01", "node-02"]

```

3. supervisor配置
```
[program:es-cluster]
user=fengjx
command=/home/fengjx/opt/elk/es/elasticsearch-7.2.0/bin/elasticsearch -E node.name=node-%(process_num)02d -E cluster.name=es-fengjx -E path.logs=logs/node-%(process_num)02d -E path.data=data/ode-%(process_num)02d-data
process_name=%(program_name)s-%(process_num)02d
numprocs=3
priority=999
startsecs=30
stopwaitsecs=5
startretries=1
stopsignal=TERM
autostart=false
autorestart=false
stdout_logfile=/home/fengjx/opt/logs/supervisor/%(program_name)s_%(process_num)02d_stdout.log
stderr_logfile=/home/fengjx/opt/logs/supervisor/%(program_name)s_%(process_num)02d_stderr.log
stderr_capture_maxbytes=1MB
directory=/home/fengjx/opt/elk/es/elasticsearch-7.2.0
```

4. 启动
```bash
sudo supervisorctl

# RUNNING表示已经启动
es-cluster:es-cluster-00         RUNNING   pid 8891, uptime 4 days, 21:26:07
es-cluster:es-cluster-01         RUNNING   pid 8890, uptime 4 days, 21:26:07
es-cluster:es-cluster-02         RUNNING   pid 8889, uptime 4 days, 21:26:07

# 启动进程
supervisor>start es-cluster:*
# 停止进程
supervisor>stop es-cluster:*
```


::: tip
如果启动时出现

[1]: max file descriptors [4096] for elasticsearch process is too low, increase to at least [65535]

可以修改supervisord的配置文件
:::
```
sudo vim /etc/supervisor/supervisord.conf

# supervisord下增加minfds配置
[supervisord]
minfds=65535
```

```
sysctl -w vm.max_map_count=262144
sysctl -a| grep vm.max_map_count

sudo vim /etc/sysctl.conf
vm.max_map_count=262144
```


```ini
[program:kibana]
user=fengjx
command=/home/fengjx/opt/elk/kibana/kibana-7.4.0-linux-x86_64//bin/kibana
directory=/home/fengjx/opt/elk/kibana/kibana-7.4.0-linux-x86_64/
autostart=false
autorestart=false
startsecs=10
stopwaitsecs=10
startretries=3
numprocs=1
stopasgroup=true
killasgroup=true
stopsignal=QUIT
stdout_logfile=/home/fengjx/opt/logs/supervisor/%(program_name)s_%(process_num)02d_stdout.log
stderr_logfile=/home/fengjx/opt/logs/supervisor/%(program_name)s_%(process_num)02d_stderr.log
environment=KIBANA_HOME=/home/fengjx/opt/elk/kibana/kibana-7.4.0-linux-x86_64
```