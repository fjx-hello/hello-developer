# idea

## 使用教程
<https://github.com/judasn/IntelliJ-IDEA-Tutorial>

## 解决编辑大文件占用大量CPU问题

idea在编辑大文件时，会占用非常多的cpu资源导致严重卡顿，使用官方jre来启动可以解决这个问题

原文链接：[Selecting the JDK version the IDE will run under](https://intellij-support.jetbrains.com/hc/en-us/articles/206544879-Selecting-the-JDK-version-the-IDE-will-run-under)

### Java 8
Please be aware that **Java 8 is required to run IntelliJ IDEA starting from version 2016** on all the supported platforms. It also applies to the lightweight IDEs based on IntelliJ platform released from the same branch (144+).

All the products on all the supported platforms already have JetBrains Runtime bundled and it will be used by default. If you need to change the runtime to a different version, see below.


### All platforms: download and install JetBrains Runtime via JB SDK Bintray Downloader plug-in

1. [Install](https://www.jetbrains.com/help/idea/installing-updating-and-uninstalling-repository-plugins.html) the [JB SDK Bintray Downloader](https://plugins.jetbrains.com/plugin/9195-jb-sdk-bintray-downloader) plugin (screenshot).
2. Start the IDE, use Help - Find Action (Ctrl+Shift+A or Cmd+Shift+A on Mac), type "[Get JB SDK](http://i.imgur.com/sWRDF9R.png)", press Enter.
3. [Select the version](http://i.imgur.com/o6tB6XP.png) to install, the higher the number after b, the more recent is the version. It makes sense to install the most recent version or the version you were asked to try by JetBrains staff.
4. Click the **Download** button and wait until the file with JetBrains Runtime is downloaded.
5. Once download is finished, click the Install button, the file will be unpacked into idea.config.path\jdks location and the full path to this JDK will be saved into idea.config.path\<product>.jdk file, for example: C:\Users\serge\.IntelliJIdea2017.1\config\jdks\jbsdk8u152b819.1_windows_x64. IDE will automatically restart.
6. Verify via About dialog that the [new runtime is used](http://i.imgur.com/5I8nxnW.png). Notice the vendor (should be JetBrains s.r.o) and the release number (should correspond to the number after b in the file you've downloaded).
7. If the JDK version in the About dialog doesn't match or didn't change, see below. Most likely, you have an environment variable configured which has the priority over the <product>.jdk file. Remove any environment variables affecting IDE runtime selection and restart.

### All platforms: switch between installed runtimes

1. Start the IDE, use Help - **Find Action** (Ctrl+Shift+A or Cmd+Shift+A on Mac), type "[Switch IDE Boot JDK](http://i.imgur.com/a5ehVBW.png)", press Enter.
2. [Switch IDE Boot JDK dialog](http://i.imgur.com/rq6AA9i.png) appears. Select the version from the drop-down or click "…" entry to specify the custom location. This location has to point to the JDK installation home directory (like c:\Program Files (x86)\Java\jdk1.8.0_112 or /Library/Java/JavaVirtualMachines/jdk1.8.0_112.jdk/Contents/Home/ or /usr/lib/jvm/open-jdk).
3. Please note that **using JetBrains Runtime is highly recommended** for performance and stability reasons (it fixes many known OpenJDK and Oracle JDK bugs).
4. Selected location is stored inside the idea.config.path\ <product>.jdk file, should you need to revert to the default behavior in case of any problem (like when IDE no longer starts), delete this file or modify the installation path inside.








