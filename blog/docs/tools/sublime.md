# sublime

## 设置eclipse快捷键
说明
```json
[
    { "keys": ["ctrl+d"], "command": "run_macro_file", "args": {"file": "res://Packages/Default/Delete Line.sublime-macro"} },//删除行
    { "keys": ["alt+/"], "command": "auto_complete" },//自动提示
    { "keys": ["alt+up"], "command": "swap_line_up" },//移动行，向上
    { "keys": ["alt+down"], "command": "swap_line_down" },
    {"keys": ["ctrl+shift+f"], "command": "reindent" , "args":{"single_line": //格式化代码，当然也可以利用html+css+js prettify插件来格式化
    false}},
    { "keys": ["ctrl+h"], "command": "show_panel", "args": {"panel": //搜索全文
    "find_in_files"} },
    { "keys": ["ctrl+shift+r"], "command": "show_overlay", "args": {"overlay": //打开工程中的某个文件
    "goto", "show_files": true} },
    { "keys": ["ctrl+up"], "command": "goto_definition" },//跳转到定义，比如在某个函数上按此键，则跳转到它的定义。
    { "keys": ["alt+left"], "command": "jump_back" },//跳转到上一个编辑处
    { "keys": ["alt+right"], "command": "jump_forward" },
    { "keys": ["ctrl+o"], "command": "show_overlay", "args": {"overlay": //跳转到当前的某个方法
    "goto", "text": "@"} },
    { "keys": ["ctrl+down"], "command": "find_under_prev" },//选中光标所在的变量或者函数，非常有用
]
```

复制内容
```json
[
    { "keys": ["ctrl+d"], "command": "run_macro_file", "args": {"file": "res://Packages/Default/Delete Line.sublime-macro"} },
    { "keys": ["alt+/"], "command": "auto_complete" },
    { "keys": ["alt+up"], "command": "swap_line_up" },
    { "keys": ["alt+down"], "command": "swap_line_down" },{"keys": ["ctrl+shift+f"], "command": "reindent" , "args":{"single_line": false}},
    { "keys": ["ctrl+h"], "command": "show_panel", "args": {"panel": "find_in_files"} },
    { "keys": ["ctrl+shift+r"], "command": "show_overlay", "args": {"overlay": "goto", "show_files": true} },
    { "keys": ["ctrl+up"], "command": "goto_definition" },
    { "keys": ["alt+left"], "command": "jump_back" },
    { "keys": ["alt+right"], "command": "jump_forward" },
    { "keys": ["ctrl+o"], "command": "show_overlay", "args": {"overlay": "goto", "text": "@"} },
    { "keys": ["ctrl+down"], "command": "find_under_prev" },
]
```
