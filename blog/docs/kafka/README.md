# kafka入门

## 安装

### 下载

<https://kafka.apache.org/quickstart#quickstart_download>

```bash
wget http://mirror.bit.edu.cn/apache/kafka/2.3.0/kafka_2.12-2.3.0.tgz
tar -xzf kafka_2.12-2.3.0.tgz
cd kafka_2.12-2.3.0
```

### 修改配置

```bash
vim config/server.properties

# 日志路径，可以配置多个，用","分隔
log.dirs=
# 服务监听地址（内网IP）
listeners=
# 服务监听地址（外网IP，可以不配置）
advertised.listeners=
# zookeeper集群配置，格式：ip:poprt,ip:port.../path
zookeeper.connect=localhost:2181/kafka
```



## 启动zookeeper

kafka依赖zookeeper服务，需要先启动zookeeper。可以另外部署zookeeper服务，或者直接使用kafka压缩包自带的zookeeper（测试使用）。

```bash
bin/zookeeper-server-start.sh config/zookeeper.properties
```


## 启动kafka server

```bash
bin/kafka-server-start.sh config/server.properties
```

默认监听端口9092

## 创建一个topic

创建一个名为hello-topic的主题

```bash
# 创建主题
bin/kafka-topics.sh --bootstrap-server localhost:9092 --create --replication-factor 1 --partitions 1 --topic hello-topic

# 查看主题列表
bin/kafka-topics.sh --bootstrap-server localhost:9092 --list
```


## 向topic发送消息

以producer角色连接kafka server，并发送消息msg1、msg2
```bash
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic hello-topic
>msg1
>msg2
>
```


## 消费topic消息

以consumer角色连接kafka server，输出生成这消息
```bash
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic hello-topic --from-beginning
msg1
msg2
```


## 删除topic

```bash
bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete  --topic hello-topic
```


