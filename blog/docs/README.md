# 优秀开源项目、博客、书籍汇总

## 博客、教程

- [并发编程网](http://ifeve.com/) 让天下没有难学的技术
- [grokonez](https://grokonez.com/) 各种软件开发教程网站，包括前端后端
- [meituan](https://tech.meituan.com) 美团技术博客
- [How-To-Ask-Questions-The-Smart-Way](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way) 提问的智慧
- [free-programming-books-zh_CN](https://github.com/justjavac/free-programming-books-zh_CN) 免费的计算机编程类中文书籍
- [awesome-programming-books](https://github.com/jobbole/awesome-programming-books) 经典编程书籍大全
- [CodingInterviews](https://github.com/gatieme/CodingInterviews) 剑指offer，计算机经典算法题
- [CS-Notes](https://github.com/CyC2018/CS-Notes) 计算机学习笔记
- [architect-awesome](https://github.com/xingshaocheng/architect-awesome) 后端架构师技术图谱
- [awesome-architecture](https://github.com/toutiaoio/awesome-architecture) 头条整理的架构师技术图谱
- [Interview-Notebook](https://github.com/CyC2018/Interview-Notebook) 计算机学习笔记
- [a-programmer-prepares](https://github.com/leohxj/a-programmer-prepares) 程序员的自我修养
- [IntelliJ-IDEA-Tutorial](https://github.com/judasn/IntelliJ-IDEA-Tutorial) IntelliJ IDEA 简体中文专题教程
- [Mac-dev-setup](https://github.com/Aaaaaashu/Mac-dev-setup) Mac 开发配置手册
- [awesome-crawler](https://github.com/BruceDone/awesome-crawler) 爬虫技术资源汇总
- [Hospital](https://github.com/open-power-workgroup/Hospital) OpenPower工作组收集汇总的莆田系医院开放数据 
- [document-style-guide](https://github.com/ruanyf/document-style-guide) 中文技术文档的写作规范
- [weekly](https://github.com/ruanyf/weekly) 阮一峰技术分享周刊
- [酷壳](https://coolshell.cn/) 耗子叔博客
- [莫那鲁道](http://thinkinjava.cn) 一个java开发者
- [vimjc](https://vimjc.com/) 专注于Vim配置、插件、Vim命令和Vim教程（一个女生维护的）
- [eng-practices](https://github.com/rootsongjc/eng-practices) 谷歌工程实践（code review）文档


## 写作

- [hugo](https://github.com/gohugoio/hugo) 使用go语言编写的静态站点生成器，github star数非常高
- [vuepress](https://github.com/vuejs/vuepress) 基于vue开发的静态站点生成器
- [docsite](https://github.com/txd-team/docsite) 一款开源站点搭建工具，能够根据markdown文档快速搭建静态站点。
- [Learning-Markdown](https://github.com/LearnShare/Learning-Markdown) Markdown 入门参考
- [hinese-copywriting-guidelines](https://github.com/sparanoid/chinese-copywriting-guidelines) 中文文案排版指北
- [nodeppt](https://github.com/ksky521/nodeppt) 一个markdown转成ppt的工具，功能非常强大
- [mpeditor](https://github.com/ksky521/mpeditor) 微信markdown编辑器

## 文档

- [Redis Command Reference](https://github.com/huangz1990/redis) 《Redis Command Reference》全文的中文翻译版

## 设计工具

- [Awesome-Design-Tools](https://github.com/LisaDziuba/Awesome-Design-Tools) 各种设计工具汇总

## 开源工具

- [SwitchHosts](https://github.com/oldj/SwitchHosts) 切换hosts文件的小程序
- [best-resume-ever](https://github.com/salomonelli/best-resume-ever) 个人简历生成工具
- [medis](https://github.com/luin/medis) Electron, React开发的redis客户端工具
- [webui-aria2](https://github.com/ziahamza/webui-aria2) aria2的web客户端
- [TProfiler](https://github.com/alibaba/TProfiler) 一个可以在生产环境长期使用的性能分析工具，长期未更新
- [redis-rdb-tools](https://github.com/sripathikrishnan/redis-rdb-tools) redis rdb文件分析工具
- [polysh](https://repo.or.cz/w/polysh.git) 可以同时连接多台服务器的ssh工具
- [hammerspoon](https://github.com/Hammerspoon/hammerspoon) 强大的macOS桌面自动化管理工具，可使用lua编写自定义功能
- [webssh](https://github.com/huashengdun/webssh) 一个基于web的ssh客户端
- [httpie](https://github.com/jakubroztocil/httpie) 一个替代curl和wget的http客户端，json和语法高亮
- [regex101](https://regex101.com/) 在线正则匹配测试
- [youtube-dl](https://github.com/rg3/youtube-dl) 视频下载工具

## 数据库

- [mysql-monthly](http://mysql.taobao.org/monthly) 数据库内核月报
- [awesome-mysql-cn](https://github.com/jobbole/awesome-mysql-cn) MySQL 资源大全中文版
- [sharding-sphere](https://github.com/sharding-sphere/sharding-sphere) 当当开源的数据库中间件，目前有京东和当当等技术在维护
- [canal](https://github.com/alibaba/canal) 阿里开源的MySQL binlog消息订阅中间件

## java

- [stackoverflow-java-top-qa](https://github.com/giantray/stackoverflow-java-top-qa) stackoverflow上Java相关回答整理翻译
- [effective-java-3rd-chinese](https://github.com/sjsdfg/effective-java-3rd-chinese) effective-java第三版中文翻译
- [arthas](https://github.com/alibaba/arthas) 阿里开源Java诊断利器
- [btrace](https://github.com/btraceio/btrace) 于Java语言的一个安全的、可提供动态追踪服务的工具
- [awesome-java](https://github.com/akullpp/awesome-java) java资源汇总
- [awesome-java-cn](https://github.com/jobbole/awesome-java-cn) Java资源大全中文版，包括开发库、开发工具、网站、博客、微信、微博等，由伯乐在线持续更新
- [advanced-java](https://github.com/doocs/advanced-java) Java工程师进阶知识完全扫盲
- [gc-handbook](https://github.com/cncounter/gc-handbook) GC参考手册-Java版
- [vertx-awesome](https://github.com/vert-x3/vertx-awesome) vertx资源汇总
- [vertx-translation-chinese](https://github.com/VertxChina/vertx-translation-chinese) vertx教程中文翻译
- [awesome-kotlin](https://github.com/KotlinBy/awesome-kotlin) kotlin资源汇总
- [Java_Books](https://github.com/zzhi/Java_Books) Java 编程书籍分享
- [java8-tutorial](https://github.com/winterbe/java8-tutorial) Java8教程
- [Java8InAction](https://github.com/java8/Java8InAction) java8 学习案例
- [java-design-patterns](https://github.com/iluwatar/java-design-patterns) java设计模式代码实现
- [design-pattern-java](https://github.com/fengjx/design-pattern-java) java24种设计模式
- [netty-learning](https://github.com/code4craft/netty-learning) Netty源码解析
- [netty-4-user-guideJava博客系统](https://github.com/waylau/netty-4-user-guide) Netty 4.x 用户指南
- [essential-netty-in-actionv](https://github.com/waylau/essential-netty-in-action) 《Netty 实战(精髓)》
- [nettybook2](https://github.com/wuyinxian124/nettybook2) 李林峰老师编写的netty权威指南（第二版）对应的源码
- [wechat-api](https://github.com/biezhi/wechat-api) 微信个人号的Java版本API
- [SpringCloud-Learning](https://github.com/dyc87112/SpringCloud-Learning) Spring Cloud教程
- [spring-cloud-study](https://github.com/eacdy/spring-cloud-study) 使用Spring Cloud与Docker实战微服务
- [Spring-Boot-Reference-Guide](https://github.com/qibaoguang/Spring-Boot-Reference-Guide) Spring Boot参考指南
- [weixin-java-tools](https://github.com/Wechat-Group/weixin-java-tools) 微信Java开发工具包
- [blade](https://github.com/lets-blade/blade) 一个有趣的基于java8的mvc框架
- [translation-spring-mvc-4-documentation](https://github.com/linesh-simplicity/translation-spring-mvc-4-documentation) Spring MVC 4.2.4.RELEASE 中文文档

## python

- [python3-docs](https://docs.python.org/zh-cn/3/) python3官方api文档
- [awesome-python-cn](https://github.com/jobbole/awesome-python-cn) Python资源大全中文版
- [python3-cookbook](https://github.com/yidao620c/python3-cookbook) python3-cookbook的中文翻译
- [wxpy](https://github.com/youfou/wxpy) 微信机器人
- [ItChat](https://github.com/littlecodersh/ItChat) 一个开源的微信个人号接口
- [pyauto](https://github.com/yorkoliu/pyauto) python自动化运维：技术与最佳实践 - 源码
- [scrapy_doc_chs](https://github.com/marchtea/scrapy_doc_chs) scrapy中文翻译文档
- [pyenv](https://github.com/pyenv/pyenv) python的依赖管理工具

## go

- [goquery](https://github.com/PuerkitoBio/goquery) 基于go实现，支持jquery语法的dom元素解析器
- [gin](https://github.com/gin-gonic/gin) 一个用Go编写的http web框架。它提供了一个类似于martini的API，具有更好的性能
- [echo](https://github.com/labstack/echo) 高性能、极简的Go web框架
- [gorm](https://github.com/jinzhu/gorm) Golang出色的ORM库
- [logrus](https://github.com/sirupsen/logrus) go的日志框架
- [jwt-go](https://github.com/dgrijalva/jwt-go) go实现的jwt
- [ini](https://github.com/go-ini/ini) go实现的ini配置文件解析
- [go-rpc-programming-guide](https://github.com/smallnest/go-rpc-programming-guide) go-rpc-programming-guide
- [gopl-zh](https://github.com/golang-china/gopl-zh) Go语言圣经中文版
- [tour](https://github.com/Go-zh/tour) Go 语言官方教程中文版

## elk

- [elasticsearch-doc-zh](https://github.com/apachecn/elasticsearch-doc-zh) apachecn翻译的elasticsearch中文文档
- [kibana-doc-zh](https://github.com/apachecn/kibana-doc-zh) apachecn翻译的kibana中文文档

## 消息中间件

- [rabbitmq-tutorials](https://github.com/rabbitmq/rabbitmq-tutorials) rabbitmq官方教程
- [kafka-doc-zh](https://github.com/apachecn/kafka-doc-zh) apachecn翻译的kafka中文文档


## 前端

- [awesome-javascript-cn](https://github.com/jobbole/awesome-javascript-cn) JavaScript 资源大全中文版
- [awesome-css-cn](https://github.com/jobbole/awesome-css-cn) CSS 资源大全中文版
- [awesome-vue](https://github.com/vuejs/awesome-vue) vue 资源大全
- [es6tutorial](https://github.com/ruanyf/es6tutorial) 阮一峰 - ECMAScript 6入门
- [jstutorial](https://github.com/ruanyf/jstutorial) 阮一峰 - js教程
- [7-days-nodejs](https://github.com/nqdeng/7-days-nodejs) 七天学会NodeJS
- [node-lessons](https://github.com/alsotang/node-lessons) Node.js 包教不包会
- [awesome-wechat-weapp](https://github.com/fengjx/awesome-wechat-weapp) 微信小程序开发资源汇总
- [awesome-egg](https://github.com/eggjs/awesome-egg) eggjs资源汇总
- [vux](https://github.com/airyland/vux) Vue & WeUI 实现的UI组件
- [fullPage](https://github.com/alvarotrigo/fullPage.js) 全屏滚动插件
- [reveal](https://github.com/hakimel/reveal.js) 网页上制作PPT
- [vue-element-admin](https://github.com/PanJiaChen/vue-element-admin) 基于vue-element的后台管理脚手架
- [editor.md](https://github.com/pandao/editor.md) 一款开源的、可嵌入的 Markdown 在线编辑器
- [2048](https://github.com/gabrielecirulli/2048) 2048游戏的H5实现
- [artDialog](https://github.com/aui/artDialog) 经典的网页对话框组件


## APM
- [apm-server](https://github.com/elastic/apm-server) elastic开源APM监控系统
- [cat](https://github.com/dianping/cat) 大众点评开源APM监控系统
- [skywalking](https://github.com/apache/skywalking) 国产开源APM监控系统，已加入apache项目


## 操作系统

- [linux-command](https://github.com/jaywcjlove/linux-command) Linux命令大全搜索工具
- [linuxtools_rst](https://github.com/me115/linuxtools_rst) Linux工具快速教程
- [Linux-Tutoria](https://github.com/judasn/Linux-Tutorial) Java程序员眼中的Linux
- [Awesome-Linux-Software](https://github.com/alim0x/Awesome-Linux-Software-zh_CN) linux上超赞的应用，软件，工具以及其它资源的集中地

## 运维

- [awesome-sysadmin-cn](https://github.com/jobbole/awesome-sysadmin-cn) 系统管理员资源大全中文版
- [docker_practice](https://github.com/yeasy/docker_practice) Docker — 从入门到实践
- [server-configs-nginx](https://github.com/h5bp/server-configs-nginx) nginx配置案例
- [open-shell-book](https://github.com/tinyclub/open-shell-book) Shell 编程范例
- [goaccess](https://github.com/allinurl/goaccess) 轻量级accesslog分析工具

## github主页

- [纯洁的微笑](https://github.com/ityouknow)
- [伯乐在线](https://github.com/jobbole)
- [阮一峰](https://github.com/ruanyf)

