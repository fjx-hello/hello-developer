var fs = require('fs');
var path = require('path');

const skipDirs = ['.git', '.vscode', 'public', 'dist', 'node_modules', '.vuepress'];
const skipFiles = [''];


module.exports = {
    treeList: function (basePath) {
        var rootDir = process.cwd();
        //解析需要遍历的文件夹
        var rootPath = path.resolve(rootDir);
        var docsPath = path.resolve(rootDir + basePath);
        var tree = []
        //调用文件遍历方法
        fileTree(tree, docsPath, rootPath);
        return tree;
    },
    listMdFile: function (basePath) {
        var fileList = [];
        var rootDir = process.cwd();
        var filePath = path.resolve(rootDir + basePath);

        var files = fs.readdirSync(filePath);
        files.forEach(function (filename) {
            //获取当前文件的绝对路径  
            var filedir = path.join(filePath, filename);
            //根据文件路径获取文件信息，返回一个fs.Stats对象  
            var stats = fs.statSync(filedir);
            var isFile = stats.isFile(); //是文件
            if (isFile && filename.endsWith(".md") && filename != 'README.md') {
                fileList.push(filename.replace('.md', ''))
            }
        })
        console.log(fileList);
        return fileList;
    },
    listDir: function (basePath) {
        var dirs = [];
        var rootDir = process.cwd();
        var filePath = path.resolve(rootDir + basePath);
        var files = fs.readdirSync(filePath);
        files.forEach(function (filename) {
            //获取当前文件的绝对路径  
            var filedir = path.join(filePath, filename);
            //根据文件路径获取文件信息，返回一个fs.Stats对象  
            var stats = fs.statSync(filedir);
            var isDir = stats.isDirectory(); //是文件夹
            if (isDir) {
                dirs.push(filename)
            }
        })
        return dirs;
    }
}


/** 
 * 文件遍历方法 
 * @param filePath 需要遍历的文件路径 
 */
function fileTree(parent, filePath, rootPath) {
    //根据文件路径读取文件，返回文件列表
    var files = fs.readdirSync(filePath);

    //遍历读取到的文件列表  
    files.forEach(function (filename) {
        // 忽略的文件夹或者文件
        if (skipDirs.indexOf(filename) > -1 || skipFiles.indexOf(filename) > -1) {
            return;
        }
        //获取当前文件的绝对路径  
        var filedir = path.join(filePath, filename);
        //根据文件路径获取文件信息，返回一个fs.Stats对象  
        var stats = fs.statSync(filedir);
        var isFile = stats.isFile(); //是文件
        var isDir = stats.isDirectory(); //是文件夹

        if (isFile && filename.endsWith(".md") && filename != 'README.md') {
            parent.push(filename.replace('.md', ''))
        } else if (isDir) {
            let children = [];
            fileTree(children, filedir, rootPath);

            parent.push({
                title: filename,
                collapsable: false,
                children
            })

        }
    });

}


/**
 * 获取相对路径
 * 
 * @param {any} rootPath  跟路径
 * @param {any} absolutePath  绝对路径
 */
function getRelativePath(rootPath, absolutePath) {
    return absolutePath.replace(`${rootPath}`, "").replace(/\\/g, '/');
}