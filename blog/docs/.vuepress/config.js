const fileKit = require('./utils/fileKit');

module.exports = {
    title: "hello-developer",
    description: '软件开发实践与总结',
    head: [
        ['link', {rel: 'icon', href: '/favicon.ico'}],
        ['meta', {name: 'apple-mobile-web-app-capable', content: 'yes'}],
        ['meta', {name: 'apple-mobile-web-app-status-bar-style', content: 'black'}]
    ],
    themeConfig: {
        // repo: 'https://gitlab.com/fjx-hello/hello-java',
        // editLinks: true,
        // docsDir: 'blog/docs',
        navbar: true,
        algolia: {
            apiKey: 'db8b5fc8b4a8d2de2d16cb662415a7e9',
            indexName: 'developer-doc-search'
        },
        nav: [
            {
                text: 'java',
                items: [
                    {text: 'java8', link: '/java/java8/date.html'},
                    {text: '网络编程', link: '/java/network/java-io.html'},
                    {text: 'jvm', link: '/java/jvm/'},
                    {text: '并发编程', link: '/java/concurrent/'},
                    // {text: '设计模式', link: '/java/patterns/'},
                    {text: '其他', link: '/java/other/'}
                    // {text: '字节码', link: '/java/bytecode/'},
                    // {text: '代码大全', link: '/java/code/'}
                ]
            },
            // {text: 'linux', link: '/linux/'},
            {text: 'zookeeper', link: '/zookeeper/'},
            {text: 'mysql', link: '/mysql/mycli.html'},
            // {text: '分布式', link: '/distributed/'},
            {text: '开发工具', link: '/tools/idea.html'},
            {text: '其他', link: '/other/How-To-Ask-Questions-The-Smart-Way.html'},
            // {text: '福利', link: '/welfare/'},
            {text: 'link', link: '/link/'},
            {text: 'about', link: '/about.html'}
        ],
        sidebar: {
            '/java/java8/': [
                {
                    title: 'java8',
                    collapsable: true,
                    children: fileKit.listMdFile('/docs/java/java8')
                }
            ],
            '/java/concurrent/thread/': [
                {
                    title: '多线程',
                    collapsable: true,
                    children: fileKit.listMdFile('/docs/java/concurrent/thread')
                }
            ],
            '/java/network/': [
                {
                    title: '网络编程',
                    collapsable: true,
                    children: fileKit.listMdFile('/docs/java/network')
                }
            ],
            '/java/other/': [
                {
                    title: '其他',
                    collapsable: true,
                    children: fileKit.listMdFile('/docs/java/other')
                }
            ],
            '/linux/': [
                {
                    title: 'linux',
                    collapsable: true,
                    children: fileKit.listMdFile('/docs/linux')
                }
            ],
            '/mysql/': [
                {
                    title: 'mysql',
                    collapsable: true,
                    children: [
                        'mycli',
                        'specification'
                    ]
                }
            ],
            '/tools/': [
                {
                    title: '开发工具',
                    collapsable: true,
                    children: fileKit.listMdFile('/docs/tools')
                }
            ],
            '/other/': [
                {
                    title: '其他',
                    collapsable: true,
                    children: fileKit.listMdFile('/docs/other')
                }
            ]
        }
    }
}
