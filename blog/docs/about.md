# 关于作者

选择比努力更重要。拿不动的东西越早放弃越好，做了减法，才能做加法，生也有涯，该放就放。专注于做自己擅长的事，享受生活。

### 联系

- [github: https://github.com/](https://github.com/fengjx)
- [email: admin@fengjx.com](mailto:admin@fengjx.com)
