# 锁


## 全局锁
全局锁会锁住整个MySQL实例，处于只读状态，所有数据更新的线程都会阻塞，包括：
- 数据更新语句（insert, update, delete）
- 数据定义语句（create, alter）
- 事务提交（commit）


```bash
flush tables with read lock
```









