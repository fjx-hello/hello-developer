# 极客时间课程优惠

<picture>
  <source srcset="/img/other/geek/mouse.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/mouse.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/leader.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/leader.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/git-github-gitlab.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/git-github-gitlab.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/java-concurrency-in-action.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/java-concurrency-in-action.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/java-core.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/java-java-core.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/jvm.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/jvm.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/DataStructures-Algorithms.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/DataStructures-Algorithms.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/go-core.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/go-core.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/linux-optimization.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/linux-optimization.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/interesting-linux.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/interesting-linux.jpeg" class="img-100" alt="Image">
</picture>

<picture>
  <source srcset="/img/other/geek/network.jpeg" media="(min-width: 719px)">
  <img src="/img/other/geek/network.jpeg" class="img-100" alt="Image">
</picture>

<style>
  @media screen and (min-width:  719px) {
    .img-100 {
       width: 220px;
    }
  }
</style>




