# hammerspoon


## hammerspoon配置

- <https://github.com/ashfinal/awesome-hammerspoon>
- <https://github.com/S1ngS1ng/HammerSpoon>
- <https://github.com/wangshub/hammerspoon-config>
- 

## hammerspoon教程

- [Lua快速入门](https://learnxinyminutes.com/docs/lua/)
- [Hammerspoon API文档](https://www.hammerspoon.org/go/)
- [Hammerspoon可以让你少买很多App](https://sspai.com/post/53992)