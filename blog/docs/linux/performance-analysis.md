# linux性能分析工具

## 性能测试工具安装
- 安装stress
```bash
sudo apt-get install stress
# 安装其他辅助工具
sudo apt-get install -y linux-tools-common linux-tools-generic linux-tools-$(uname -r)）
```
- 模拟cpu密集型进程
```bash
# 模拟1个cpu使用率100%
stress --cpu 1 --timeout 600
```

- 模拟I/O密集型进程
```bash
stress -i 1 --timeout 600
```

## cpu

### 性能分析工具

转自：<https://time.geekbang.org/column/article/72685>

- 通过指标查询

性能指标 | 工具 | 说明
---------|----------|---------
 平均负载 | uptime <br> top | uptime比较简单 <br> top提供了更全面的数据
 系统整体cpu使用率 | vmstat <br> mpstat <br> top <br> sar <br> /proc/stat | top、vmstat、mpstat只可以动态查看，2️而sar可以记录历史数据 <br> /proc/stat是其他性能工具的数据来源
 进程cpu使用率 | top <br> pidstat <br> ps <br> htop <br> atop | top和ps可以根据cpu使用率进行排序，而pidstat只显示实际用了cpu的进程 <br>htop和atop界面更直观
 系统上下文切换 | vmstat | 除了上下文切换，还可以查看运行状态和不可中断状态的进程数 
 进程上下文切换 | pidstat | 注意加上 -w选项


- 通过工具查询

性能工具 | CPU性能指标 | 说明
---------|----------|---------
 uptime | 平均负载 | 
 top | 平均负载、整体CPU使用情率以及每个进程的状态和cpu使用率、运行队列 | 
 htop | top增强版、以不同颜色区分不同类型的进程，更直观 | 
 atop | cpu、内存、磁盘和网络等各种资源的全面监控 | 
 vmstat | 系统整体cpu使用率、上下文切换次数、中断次数、还包括运处于行和不可中断进程数 | 
 mpstat | 每个cpu的使用率和软中断次数 | 
 pidstat | 进程和线程cpu使用率、中断上下文切换次数 |
 /proc/softirps | 软中断类型和在每个cpu上累计的次数 |
 /proc/interrupts | 硬中断类型和在每个cpu上累计的次数 |
 ps | 每个进程的状态和cpu使用率 | 
 pstree | 进程的父子关系 |
 dstat | 系统整体cpu使用率 |
 sar |  系统整体cpu使用率，包括可配置的历史数据 | 
 strace | 进程的系统调用 | 
 perf | cpu性能事件剖析，如调用链分析、cpu缓存、cpu调度率 | 
 exescsnoop | 监控短时进程 | 



### pidstat
```bash
# 每隔5秒输出一组数据
pidstat -u 5 1
Linux 4.15.0-29deepin-generic (fengjx-deepin) 	2019年06月11日 	_x86_64_	(4 CPU)

20时55分12秒   UID       PID    %usr %system  %guest   %wait    %CPU   CPU  Command
20时55分17秒     0       215    0.00    3.59    0.00    0.00    3.59     1  kworker/1:1H
20时55分17秒     0      1514    0.00    0.20    0.00    0.00    0.20     2  Xorg
20时55分17秒   116      1542    0.00    0.20    0.00    0.00    0.20     0  redis-server
20时55分17秒  1000      5630    0.00    0.20    0.00    0.00    0.20     0  deepin-wm
20时55分17秒  1000      5799    0.20    0.00    0.00    0.00    0.20     0  dde-desktop
20时55分17秒     0     28262    0.20    0.20    0.00    0.00    0.40     0  watch
20时55分17秒     0     28352    0.00    0.20    0.00    0.00    0.20     2  mpstat
20时55分17秒     0     30743    0.40   24.95    0.00    5.59   25.35     1  stress
20时55分17秒     0     31405    0.00    0.20    0.00    0.00    0.20     3  pidstat

平均时间:   UID       PID    %usr %system  %guest   %wait    %CPU   CPU  Command
平均时间:     0       215    0.00    3.59    0.00    0.00    3.59     -  kworker/1:1H
平均时间:     0      1514    0.00    0.20    0.00    0.00    0.20     -  Xorg
平均时间:   116      1542    0.00    0.20    0.00    0.00    0.20     -  redis-server
平均时间:  1000      5630    0.00    0.20    0.00    0.00    0.20     -  deepin-wm
平均时间:  1000      5799    0.20    0.00    0.00    0.00    0.20     -  dde-desktop
平均时间:     0     28262    0.20    0.20    0.00    0.00    0.40     -  watch
平均时间:     0     28352    0.00    0.20    0.00    0.00    0.20     -  mpstat
平均时间:     0     30743    0.40   24.95    0.00    5.59   25.35     -  stress
平均时间:     0     31405    0.00    0.20    0.00    0.00    0.20     -  pidstat

```

参数 | 含义 | 
--------|----------
%usr    | 用户态cpu使用率
%system | 内核态cpu使用率
%wait   | 等待cpu
&CPU    | cpu使用率


- vmstat






