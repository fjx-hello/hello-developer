# linux常用命令


参考：
<https://coolshell.cn/articles/19219.html>
<https://github.com/alebcay/awesome-shell>
<https://github.com/awesome-lists/awesome-bash>

## 常用alias
```bash
alias ll='ls -la'
alias svim='sudo vim'
alias mkcd='mkcd(){ mkdir -p "$1"; cd "$1" }; mkcd '
alias ..="cd .."
alias ...="cd ../../"
alias ....="cd ../../../"
alias .....="cd ../../../../"

```

## 常用命令
```bash
# 查看进程信息 
ls -l /proc/$pid
# 进程启动命令
cat /proc/$pid/cmdline 进程启动命令
# 进程工作目录
ls -l /proc/$pid/cwd
# 进程环境变量
cat /proc/$pid/environ
# 进程执行命令文件
ls -l /proc/$pid/exe
# 进程文件描述符
ls -l /proc/$pid/fd
# 进程内存映射
cat /proc/$pid/maps
# 进程持有内存
cat /proc/$pid/mem
# 进程根目录
ls -l /proc/$pid/root
# 进程状态
cat /proc/$pid/stat
# 进程内存状态
cat /proc/$pid/statm
# 进程状态信息
cat /proc/$pid/status

```


