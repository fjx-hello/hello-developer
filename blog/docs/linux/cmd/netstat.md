## netstat

### 查看连接到服务器top10客户端的IP
```bash
netstat -nat | awk '{print $5}' | awk -F ':' '{print $1}' | sort | uniq -c | sort -rn | head -n 10
```


