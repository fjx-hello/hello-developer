# linux 常用命令

## CPU

- 查看cpu信息
```bash
# 查看cpu核心
grep 'model name' /proc/cpuinfo

# 查看cpu核心数
grep 'model name' /proc/cpuinfo | wc -l
```

- open files
```bash
# 查看所有文件限制参数
ulimit -a

# 查看进程open files
cat /proc/{pid}/limits
cat /proc/1237/limits | wc -l

# 查看已打开的文件描述符
lsof -p {pid} | wc -l

```


