#!/usr/bin/env sh

# 确保脚本抛出遇到的错误
set -e

cd blog

# 生成静态文件
npm run build

cd docs/.vuepress/dist

# 如果是发布到自定义域名
echo 'blog.fengjx.com' > CNAME
echo '<http://blog.fengjx.com>' > README.md

git init
git add .
git commit -m 'deploy'

git push -f git@github.com:fengjx/fengjx.github.io.git master:master

cd ..
rm -rf dist