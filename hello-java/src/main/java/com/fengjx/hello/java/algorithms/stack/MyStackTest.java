package com.fengjx.hello.java.algorithms.stack;

/**
 * @author fengjianxin
 */
public class MyStackTest {


    public static void main(String[] args) {

        MyStack<Integer> stack = new MyStack<>(10);

        for (int i = 0; i < 10; i++) {
            stack.push(i);
            System.out.println(stack);
        }

        System.out.println("peek1: " + stack.peek());
        System.out.println("pop1: " + stack.pop());
        System.out.println("pop2: " + stack.pop());
        System.out.println("peek2: " + stack.peek());
        System.out.println(stack);

    }


}
