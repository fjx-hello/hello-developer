package com.fengjx.hello.java.juc.lock;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 读写锁
 * http://ifeve.com/java-%e8%af%bb%e5%86%99%e9%94%81%e7%9a%84%e5%ae%9e%e7%8e%b0%e5%8e%9f%e7%90%86/
 * http://ifeve.com/basic-thread-synchronization-6/
 *
 * @author fengjianxin
 * @version 2019/12/24
 */
public class ReadWriteLockTest {


    public static void main(String[] args) {
        PricesInfo pricesInfo = new PricesInfo();

        List<Thread> threadsReader = Stream.generate(() -> new Thread(new Reader(pricesInfo)))
                .limit(5)
                .collect(Collectors.toList());

        Writer writer = new Writer(pricesInfo);
        Thread threadWriter = new Thread(writer);

        threadsReader.forEach(Thread::start);
        threadWriter.start();
    }

    private static class PricesInfo {
        private ReadWriteLock lock;

        private double price1;
        private double price2;

        public PricesInfo() {
            this.lock = new ReentrantReadWriteLock();
            this.price1 = 1.0D;
            this.price2 = 2.0D;
        }

        public double getPrice1() {
            lock.readLock().lock();
            System.out.printf("start %s: Price 1: %f\n", Thread.currentThread().getName(), price1);
            double value = price1;
            System.out.printf("start %s: Price 1: %f\n", Thread.currentThread().getName(), value);
            lock.readLock().unlock();
            return value;
        }

        public double getPrice2() {
            lock.readLock().lock();
            System.out.printf("start %s: Price 2: %f\n", Thread.currentThread().getName(), price2);
            double value = price2;
            System.out.printf("end %s: Price 2: %f\n", Thread.currentThread().getName(), value);
            lock.readLock().unlock();
            return value;
        }

        public void setPrices(double price1, double price2) {
            lock.writeLock().lock();
            System.out.printf("%s: set Price 1: %f\n", Thread.currentThread().getName(), price1);
            this.price1 = price1;
            System.out.printf("%s: set Price 2: %f\n", Thread.currentThread().getName(), price2);
            this.price2 = price2;
            System.out.printf("%s: Writer: Prices have been modified.\n", Thread.currentThread().getName());
            lock.writeLock().unlock();
        }


    }

    private static class Reader implements Runnable {
        private PricesInfo pricesInfo;

        public Reader(PricesInfo pricesInfo) {
            this.pricesInfo = pricesInfo;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                pricesInfo.getPrice1();
                pricesInfo.getPrice2();
            }
        }
    }

    private static class Writer implements Runnable {
        private PricesInfo pricesInfo;

        public Writer(PricesInfo pricesInfo) {
            this.pricesInfo = pricesInfo;
        }

        @Override
        public void run() {
            for (int i = 0; i < 3; i++) {
                pricesInfo.setPrices(Math.random() * 10, Math.random() * 8);
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}