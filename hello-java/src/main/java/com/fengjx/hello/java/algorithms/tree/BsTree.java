package com.fengjx.hello.java.algorithms.tree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Consumer;

/**
 * Binary Search Tree - 二分搜索树
 *
 * @author fengjianxin
 */
public class BsTree<E extends Comparable<E>> {

    private Node root;
    private int size;

    public BsTree() {
        this.root = null;
        this.size = 0;
    }

    public void add(E e) {
        this.root = add(this.root, e);
    }

    private Node add(Node node, E e) {
        if (node == null) {
            size++;
            return new Node(e);
        }
        if (e.compareTo(node.e) > 0) {
            node.right = add(node.right, e);
        } else if (e.compareTo(node.e) < 0) {
            node.left = add(node.left, e);
        }
        return node;
    }

    /**
     * 删除任意元素
     */
    public void remove(E e) {
        if (e == null) {
            return;
        }
        this.root = removeNode(this.root, e);
    }


    private Node removeNode(Node node, E e) {
        if (node == null) {
            return null;
        }
        // 删除元素大于节点元素
        if (e.compareTo(node.e) > 0) {
            node.right = removeNode(node.right, e);
            return node;
        } else if (e.compareTo(node.e) < 0) { // 删除元素小于节点元素
            node.left = removeNode(node.left, e);
            return node;
        } else { // 删除元素等于节点元素
            // 左节点为空，则被删除节点的右子树就作为断开节点的跟节点
            if (node.left == null) {
                final Node right = node.right;
                node.right = null;
                size--;
                return right;
            }
            // 右节点为空，则被删除节点的做子树就作为断开节点的跟节点
            if (node.right == null) {
                final Node left = node.left;
                node.left = null;
                size--;
                return left;
            }

            // 当前节点既有右子树，又有做子树
            // 获得后继节点（也可以用前驱节点）
            final Node target = minNode(node.right);
            target.right = removeMinNode(node.right);
            target.left = node.left;
            node.right = null;
            node.left = null;
            return target;
        }
    }

    public E removeMax() {
        final E max = max();
        this.root = removeMaxNode(this.root);
        return max;
    }

    /**
     * 递归判断如果节点右子树是null，则当前节点是最大值
     *
     * @return 被删除节点数的左节点
     */
    private Node removeMaxNode(Node node) {
        if (node.right == null) {
            final Node left = node.left;
            // node.left = null;
            size--;
            return left;
        }
        node.right = removeMaxNode(node.right);
        return node;
    }

    public E removeMin() {
        final E min = min();
        this.root = removeMinNode(this.root);
        return min;
    }

    /**
     * 递归判断如果节点右子树是null，则当前节点是最大值
     *
     * @return 被删除节点数的右节点
     */
    private Node removeMinNode(Node node) {
        if (node.left == null) {
            final Node right = node.right;
            // node.right = null;
            size--;
            return right;
        }
        node.left = removeMinNode(node.left);
        return node;
    }

    /**
     * 判断元素是否存在
     */
    public boolean contains(E e) {
        return search(this.root, e);
    }

    private boolean search(Node node, E e) {
        if (e == null || node == null) {
            return false;
        }
        if (e.compareTo(node.e) == 0) {
            return true;
        } else if (e.compareTo(node.e) < 0) {
            return search(node.left, e);
        } else {
            return search(node.right, e);
        }
    }

    /**
     * 最大节点
     */
    public E max() {
        return maxNode(this.root).e;
    }

    private Node maxNode(Node node) {
        if (node.right == null) {
            return node;
        }
        return maxNode(node.right);
    }

    /**
     * 最小节点
     */
    public E min() {
        return minNode(this.root).e;
    }

    private Node minNode(Node node) {
        if (node.left == null) {
            return node;
        }
        return minNode(node.left);
    }

    /**
     * 前序遍历，先遍历左节点
     */
    public void preForEach(Consumer<E> consumer) {
        preForEach(this.root, consumer);
    }

    private void preForEach(Node node, Consumer<E> consumer) {
        if (node != null) {
            consumer.accept(node.e);
            preForEach(node.left, consumer);
            preForEach(node.right, consumer);
        }
    }

    public void inForEach(Consumer<E> consumer) {
        inForEach(this.root, consumer);
    }

    /**
     * 中序遍历，结果刚好从小到大排序
     */
    private void inForEach(Node node, Consumer<E> consumer) {
        if (node != null) {
            inForEach(node.left, consumer);
            consumer.accept(node.e);
            inForEach(node.right, consumer);
        }
    }


    public void postForEach(Consumer<E> consumer) {
        postForEach(this.root, consumer);
    }

    /**
     * 后序遍历
     */
    private void postForEach(Node node, Consumer<E> consumer) {
        if (node != null) {
            postForEach(node.left, consumer);
            postForEach(node.right, consumer);
            consumer.accept(node.e);
        }
    }

    /**
     * 层序遍历
     */
    public void levelForEach(Consumer<E> consumer) {
        levelForEach(this.root, consumer);
    }

    private void levelForEach(Node node, Consumer<E> consumer) {
        Queue<Node> queue = new LinkedList<>();
        queue.add(node);
        while (!queue.isEmpty()) {
            Node n = queue.remove();
            consumer.accept(n.e);
            if (null != n.left) {
                queue.add(n.left);
            }
            if (null != n.right) {
                queue.add(n.right);
            }
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private class Node {
        private E e;
        private Node left;
        private Node right;

        public Node(E e) {
            this.e = e;
            this.left = null;
            this.right = null;
        }
    }

}
