package com.fengjx.hello.java.algorithms.stack;

import com.fengjx.hello.java.algorithms.list.MyArrayList;

/**
 * 自定义栈
 *
 * @author fengjianxin
 */
public class MyStack<E> {


    private transient MyArrayList<E> array;

    public MyStack() {
        array = new MyArrayList<>(10);
    }

    public MyStack(int size) {
        array = new MyArrayList<>(size);
    }

    /**
     * 入栈
     */
    public void push(E e) {
        array.add(e);
    }

    /**
     * 出栈
     */
    public E pop() {
        return array.remove(array.size() - 1);
    }

    /**
     * 查看栈顶元素
     */
    public E peek() {
        return array.getLast();
    }

    public boolean empty() {
        return array.isEmpty();
    }

    public int size() {
        return array.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("stack size: ").append(array.size()).append(" ==> ");
        if (array.size() == 0) {
            return "[]";
        }
        sb.append('[');
        for (int i = 0; i < array.size(); i++) {
            Object e = array.get(i);
            sb.append(e == this ? "(this stack)" : e);
            if (i + 1 < array.size()) {
                sb.append(',').append(' ');
            }
        }
        return sb.append(']').toString();
    }

}
