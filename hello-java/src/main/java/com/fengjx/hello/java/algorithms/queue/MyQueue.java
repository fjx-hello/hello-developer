package com.fengjx.hello.java.algorithms.queue;

import com.fengjx.hello.java.algorithms.list.MyArrayList;

/**
 * 自定义实现基于数组的队列
 *
 * @author fengjianxin
 */
public class MyQueue<E> {


    private transient MyArrayList<E> array;

    public MyQueue() {
        array = new MyArrayList<>(10);
    }

    public MyQueue(int size) {
        array = new MyArrayList<>(size);
    }

    public void add(E e) {
        array.add(e);
    }

    public E poll() {
        return array.remove(0);
    }

    public E peek() {
        return array.getFirst();
    }


    public boolean empty() {
        return array.isEmpty();
    }

    public int size() {
        return array.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("queue size: ").append(array.size()).append(" ==> ");
        if (array.size() == 0) {
            return "[]";
        }
        sb.append('[');
        for (int i = 0; i < array.size(); i++) {
            Object e = array.get(i);
            sb.append(e == this ? "(this queue)" : e);
            if (i + 1 < array.size()) {
                sb.append(',').append(' ');
            }
        }
        return sb.append(']').toString();
    }

}
