package com.fengjx.hello.java.juc;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author fengjianxin
 */
public class CountDownLatchTest {

    private static final int COUNT = 10;

    private static final AtomicInteger DOWN = new AtomicInteger(0);

    private static final CountDownLatch LATCH = new CountDownLatch(COUNT);

    public static void main(String[] args) throws InterruptedException {
        List<Thread> threads = Stream.generate(() -> new Thread(() -> {
            System.out.printf("countDown: %d \n", DOWN.incrementAndGet());
            LATCH.countDown();
        })).limit(COUNT).collect(Collectors.toList());

        for (Thread thread : threads) {
            thread.start();
        }

        LATCH.await();
        System.out.println("countDown 为0，程序执行结束");


    }


}
