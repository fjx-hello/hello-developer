package com.fengjx.hello.java.algorithms.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/unique-morse-code-words/
 *
 * @author fengjianxin
 */
public class LeetCode804 {

    public static void main(String[] args) {
        Solution solution = new Solution();
        String[] words = {"gin", "zen", "gig", "msg"};
        System.out.println(solution.uniqueMorseRepresentations(words));
    }

    static class Solution {

        private final String[] codes = new String[]{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};

        public int uniqueMorseRepresentations(String[] words) {
            Set<String> set = new HashSet<>();
            for (String word : words) {
                StringBuilder sb = new StringBuilder(20);
                for (char c : word.toCharArray()) {
                    // 'a'作为起始编号，a-z的ascii码是连续的
                    sb.append(codes[c - 'a']);
                }
                set.add(sb.toString());
            }
            return set.size();
        }
    }

}
