package com.fengjx.hello.java.algorithms.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 求两个数组的交集
 * https://leetcode-cn.com/problems/intersection-of-two-arrays/
 *
 * @author fengjianxin
 */
public class LeetCode349 {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] arr1 = new int[]{1, 2, 3, 6, 3, 5, 10, 22, 33, 44, 55, 66};
        int[] arr2 = new int[]{1, 2, 8, 4, 9, 5};
        int[] ints = solution.intersection(arr1, arr2);
        for (int i : ints) {
            System.out.println(i);
        }
    }

    static class Solution {

        public int[] intersection(int[] nums1, int[] nums2) {
            Set<Integer> set1 = new HashSet<>();
            for (int n1 : nums1) {
                set1.add(n1);
            }

            Set<Integer> set2 = new HashSet<>();
            for (int n2 : nums2) {
                set2.add(n2);
            }

            if (set1.size() > set2.size()) {
                return intersection(set2, set1);
            } else {
                return intersection(set1, set2);
            }
        }


        private int[] intersection(Set<Integer> set1, Set<Integer> set2) {
            int[] array = new int[set1.size()];
            int idx = 0;
            for (Integer i : set1) {
                if (set2.contains(i)) {
                    array[idx++] = i;
                }
            }
            return Arrays.copyOf(array, idx);
        }
    }
}
