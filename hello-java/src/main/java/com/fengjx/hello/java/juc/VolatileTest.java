package com.fengjx.hello.java.juc;

import java.util.concurrent.TimeUnit;

/**
 * @author fengjianxin
 */
public class VolatileTest implements Runnable {

//    private boolean start = true;
    private volatile boolean start = true;


    public void stop() {
        start = false;
    }


    @Override
    public void run() {
        long begin = System.currentTimeMillis();
        while (start) {
        }
        System.out.println("stop..." + (System.currentTimeMillis() - begin));
    }

    public static void main(String[] args) throws InterruptedException {
        VolatileTest test = new VolatileTest();
        new Thread(test).start();
        TimeUnit.MILLISECONDS.sleep(100);
        test.stop();
    }

}
