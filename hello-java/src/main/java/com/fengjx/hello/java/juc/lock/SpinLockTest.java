package com.fengjx.hello.java.juc.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 自旋锁
 *
 * @author fengjianxin
 * @version 2019/12/24
 */
public class SpinLockTest {

    private SpinLock spinLock = new SpinLock();


    public void method1() {
        spinLock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " - method1 start");
            TimeUnit.SECONDS.sleep(1);
            System.out.println(Thread.currentThread().getName() + " - method1 end");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            spinLock.unlock();
        }
    }

    public void method2() {
        spinLock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " - method2 start");
            TimeUnit.SECONDS.sleep(1);
            System.out.println(Thread.currentThread().getName() + " - method2 end");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            spinLock.unlock();
        }
    }


    public static void main(String[] args) throws InterruptedException {

        SpinLockTest test = new SpinLockTest();

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                test.method1();
            }, "thread" + i).start();
        }

        TimeUnit.SECONDS.sleep(12);
    }

    /**
     * 非可重入自旋锁
     */
    private static class SpinLock {
        private AtomicReference<Thread> sign = new AtomicReference<>();

        public void lock() {
            Thread current = Thread.currentThread();
            while (!sign.compareAndSet(null, current)) {
            }
        }


        public void unlock() {
            Thread current = Thread.currentThread();
            sign.compareAndSet(current, null);
        }
    }

    /**
     * 可重入自旋锁
     */
    private static class SpinReentrantLock {

        private AtomicReference<Thread> owner = new AtomicReference<>();
        // 可重入锁，需要记录锁次数（锁多少次就要释放多少次）
        private int count = 0;

        public void lock() {
            Thread current = Thread.currentThread();
            if (current == owner.get()) {
                return;
            }
            while (!owner.compareAndSet(null, current)) {
            }
        }


        public void unlock() {
            Thread current = Thread.currentThread();
            if (current == owner.get()) {
                if (count != 0) {
                    count--;
                } else {
                    owner.compareAndSet(current, null);
                }

            }
        }
    }
}
