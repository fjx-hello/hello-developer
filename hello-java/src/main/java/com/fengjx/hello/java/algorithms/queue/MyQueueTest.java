package com.fengjx.hello.java.algorithms.queue;

import java.util.ArrayDeque;

/**
 * @author fengjianxin
 */
public class MyQueueTest {


    public static void main(String[] args) {
        ArrayDeque<String> q = new ArrayDeque<>();
        q.add("a");
        q.add("b");
        q.add("c");
        q.add("d");
        System.out.println(q.poll());
        System.out.println(q.peek());
        System.out.println(q);


        MyQueue<String> mq = new MyQueue<>();
        mq.add("a");
        mq.add("b");
        mq.add("c");
        mq.add("d");
        System.out.println(mq.poll());
        System.out.println(mq.peek());
        System.out.println(mq);
    }


}
