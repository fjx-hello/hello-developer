package com.fengjx.hello.java.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author fengjianxin
 */
public class AtomicTest {

    private static final int COUNT = 10;

    private static final AtomicInteger ATOMIC_INTEGER = new AtomicInteger();
    private static final AtomicReference<Integer> ATOMIC_REFERENCE = new AtomicReference<>(0);

    private static final CountDownLatch LATCH = new CountDownLatch(COUNT);

    public static void main(String[] args) throws InterruptedException {
        // testAtomicInteger();
        testAtomicReference();
    }

    private static void testAtomicInteger() throws InterruptedException {
        for (int i = 0; i < COUNT; i++) {
            int finalI = i;
            new Thread(() -> {
                int curr = ATOMIC_INTEGER.addAndGet(finalI);
                System.out.println(curr);
                LATCH.countDown();
            }).start();
        }
        LATCH.await();
        System.out.println("SUM: " + ATOMIC_INTEGER.get());
    }

    private static void testAtomicReference() {
        ATOMIC_REFERENCE.compareAndSet(0, 10);
        ATOMIC_REFERENCE.compareAndSet(1, 20);
        ATOMIC_REFERENCE.compareAndSet(10, 30);
        ATOMIC_REFERENCE.compareAndSet(20, 40);
        System.out.println(ATOMIC_REFERENCE.get());
    }

}
