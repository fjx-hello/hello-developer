package com.fengjx.hello.java.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fengjianxin
 * @version 2019-04-17
 */
public class JvmOOM {

    /**
     * -server -Xms20m -Xmx20m -XX:+PrintGCTimeStamps -XX:+PrintGCDetails -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -Xloggc:jvmoom-gc.log -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./jvmoom.hprof
     */
    public static void main(String[] args) {

        long maxMemory = Runtime.getRuntime().maxMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("maxMemory: " + maxMemory / 1024 / 1024 + "m");
        System.out.println("totalMemory: " + totalMemory / 1024 / 1024 + "m");

        List<byte[]> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(new byte[1024 * 1024]);
        }

    }

}
