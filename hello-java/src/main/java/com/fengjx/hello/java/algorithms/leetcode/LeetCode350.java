package com.fengjx.hello.java.algorithms.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fengjianxin
 */
public class LeetCode350 {


    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] arr1 = new int[]{4, 9, 5, 5, 5, 6};
        int[] arr2 = new int[]{9, 4, 9, 8, 4, 5, 5};
        int[] ints = solution.intersect(arr1, arr2);
        for (int i : ints) {
            System.out.println(i);
        }
    }


    static class Solution {
        public int[] intersect(int[] nums1, int[] nums2) {
            if (nums1.length < nums2.length) {
                Map<Integer, Integer> map = arrayToMap(nums2);
                return intersect(nums1, map);
            } else {
                Map<Integer, Integer> map = arrayToMap(nums1);
                return intersect(nums2, map);
            }
        }

        private int[] intersect(int[] nums, Map<Integer, Integer> map) {
            int[] array = new int[nums.length];
            int idx = 0;

            for (int num : nums) {
                Integer val = map.get(num);
                if (val != null && val > 0) {
                    array[idx++] = num;
                    val = val - 1;
                    if (val == 0) {
                        map.remove(num);
                    } else {
                        map.put(num, val);
                    }
                }

            }
            return Arrays.copyOf(array, idx);
        }

        private Map<Integer, Integer> arrayToMap(int[] nums) {
            Map<Integer, Integer> map = new HashMap<>(nums.length * 100 / 75);
            Integer num;
            for (int n : nums) {
                num = map.get(n);
                if (num != null) {
                    map.put(n, num + 1);
                } else {
                    map.put(n, 1);
                }
            }
            return map;
        }


    }


}
