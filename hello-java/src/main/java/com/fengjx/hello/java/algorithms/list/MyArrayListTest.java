package com.fengjx.hello.java.algorithms.list;

import java.util.ArrayList;

/**
 * @author fengjianxin
 */
public class MyArrayListTest {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(10);
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.remove("a");
        System.out.println(list);

        MyArrayList<String> myList = new MyArrayList<>(10);
        myList.add("a");
        myList.add("b");
        myList.add("c");
        myList.add("d");
        myList.add("e");
        myList.add("f");
        myList.remove("a");
        myList.remove("f");
        System.out.println(myList);
    }




}
