package com.fengjx.hello.java.juc.lock;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * 可重入锁
 * 一个线程中可以多次获取同一把锁，避免死锁情况的发生
 *
 * @author fengjianxin
 * @version 2019/12/24
 */
public class ReentrantLockTest {

    private Lock lock = new ReentrantLock();


    public void lockMethod1() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " - method1");
            TimeUnit.SECONDS.sleep(3);
            lockMethod2();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void lockMethod2() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " - method2");
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }


    public static void main(String[] args) {
        ReentrantLockTest test = new ReentrantLockTest();

        new Thread(test::lockMethod2, "thread1").start();
        new Thread(test::lockMethod1, "thread2").start();

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
