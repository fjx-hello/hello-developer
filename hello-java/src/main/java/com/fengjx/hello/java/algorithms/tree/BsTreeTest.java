package com.fengjx.hello.java.algorithms.tree;

/**
 * @author fengjianxin
 */
public class BsTreeTest {

    public static void main(String[] args) {
        BsTree<Integer> tree = new BsTree<>();
        int[] arr = new int[]{41, 33, 40, 6, 75, 22, 55, 77};
        for (int i : arr) {
            tree.add(i);
        }
        System.out.println("max: " + tree.max());
        System.out.println("min: " + tree.min());
        // 通过前序遍历打印
        tree.preForEach(System.out::println);
        System.out.println("====================");
        // 中序遍历
        tree.inForEach(System.out::println);
        System.out.println("====================");
        // 后序遍历
        tree.postForEach(System.out::println);
        System.out.println("====================");
        // 层序遍历
        tree.levelForEach(System.out::println);
        System.out.println("==========removeMax==========");
        tree.removeMax();
        // 层序遍历
        tree.inForEach(System.out::println);
        System.out.println("==========removeMMin==========");
        tree.removeMin();
        // 层序遍历
        tree.inForEach(System.out::println);
        System.out.println("==========remove==========");
        tree.remove(41);
        // 层序遍历
        tree.levelForEach(System.out::println);

    }

}
