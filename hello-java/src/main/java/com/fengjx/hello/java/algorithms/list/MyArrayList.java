package com.fengjx.hello.java.algorithms.list;

import java.util.Arrays;

/**
 * 自定义ArrayList
 *
 * @author fengjianxin
 */
public class MyArrayList<E> {

    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
    private static final Object[] EMPTY_ELEMENTDATA = {};

    private transient Object[] elementData;
    private int size = 0;


    public MyArrayList() {
        this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
    }

    public MyArrayList(int initialCapacity) {
        if (initialCapacity > 0) {
            this.elementData = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.elementData = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
    }

    public boolean add(E element) {
        // 判断是否需要扩容
        ensureCapacityInternal(size + 1);
        // size + 1，并且往数组最后一位添加元素
        elementData[size++] = element;
        return true;
    }

    public void add(int index, E element) {
        rangeCheckForAdd(index);
        // 判断是否需要扩容
        ensureCapacityInternal(size + 1);
        // index之后的所有元素都往后移动一位
        System.arraycopy(elementData, index, elementData, index + 1,
                size - index);

        elementData[index] = element;
    }

    public E getLast() {
        return getElementData(size - 1);
    }

    public E getFirst() {
        return getElementData(0);
    }

    public E get(int index) {
        return getElementData(index);
    }

    public boolean remove(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    fastRemove(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(elementData[i])) {
                    fastRemove(i);
                    return true;
                }
            }
        }
        return false;
    }


    private void fastRemove(int index) {
        // 计算需要拷贝数组的个数，即：从索引下标开始，还有多少个元素
        int numMoved = size - index - 1;

        if (numMoved > 0) {
            // 拷贝索引后面所有元素，往前移动1位来覆盖掉被删除的元素
            System.arraycopy(elementData, index + 1, elementData, index, numMoved);
        }
        // 数组拷贝之后，最后一个元素是要删除的元素，或者是数组拷贝之后多余的元素，需要删掉
        elementData[--size] = null;
    }

    public E remove(int index) {
        rangeCheck(index);
        // 获取要删除的元素
        E oldValue = getElementData(index);
        fastRemove(index);
        return oldValue;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }


    private E getElementData(int index) {
        return (E) elementData[index];
    }

    private void ensureCapacityInternal(int minCapacity) {
        if (minCapacity - elementData.length > 0) {
            // 这里要考虑最大值超过Integer.MAX_VALUE的问题
            int newCapacity = minCapacity * 2;
            elementData = Arrays.copyOf(elementData, newCapacity);
        }
    }

    /**
     * 检查下标合法性
     */
    private void rangeCheckForAdd(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    /**
     * 检查下标是否大于list长度
     */
    private void rangeCheck(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("array size: ").append(size).append(" ==> ");
        if (size == 0) {
            return "[]";
        }
        sb.append('[');
        for (int i = 0; i < size; i++) {
            Object e = elementData[i];
            sb.append(e == this ? "(this Collection)" : e);
            if (i + 1 < size) {
                sb.append(',').append(' ');
            }
        }
        return sb.append(']').toString();
    }
}
