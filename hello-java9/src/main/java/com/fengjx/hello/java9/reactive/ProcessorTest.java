package com.fengjx.hello.java9.reactive;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

/**
 * 使用Processor转换流
 *
 * @author fengjianxin
 */
public class ProcessorTest {

    private static final int count = 100;
    private static final CountDownLatch latch = new CountDownLatch(count);

    public static void main(String[] args) {
        SubmissionPublisher<Integer> pub = null;
        try {
            pub = new SubmissionPublisher<>();

            MyProcessor processor = new MyProcessor();

            Flow.Subscriber<String> sub = new Flow.Subscriber<>() {

                private Flow.Subscription subscription;

                @Override
                public void onSubscribe(Flow.Subscription subscription) {
                    this.subscription = subscription;
                    this.subscription.request(10);
                }

                @Override
                public void onNext(String item) {
                    System.out.println(item);
                    this.subscription.request(1);
                    latch.countDown();
                }

                @Override
                public void onError(Throwable throwable) {
                    throwable.printStackTrace();
                    this.subscription.cancel();
                }

                @Override
                public void onComplete() {
                    System.out.println("sub onComplete");
                }
            };

            pub.subscribe(processor);
            processor.subscribe(sub);

            for (int i = 0; i < count; i++) {
                pub.submit(i);
            }
            latch.await();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (pub != null) {
                pub.close();
            }
        }

    }


    private static class MyProcessor extends SubmissionPublisher<String> implements Flow.Processor<Integer, String> {
        private Flow.Subscription subscription;

        @Override
        public void onSubscribe(Flow.Subscription subscription) {
            this.subscription = subscription;
            this.subscription.request(10);
        }

        @Override
        public void onNext(Integer item) {
            this.submit("rec->" + item);
            this.subscription.request(1);
        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
            this.subscription.cancel();
        }

        @Override
        public void onComplete() {
            System.out.println("processor onComplete");
        }
    }


}

