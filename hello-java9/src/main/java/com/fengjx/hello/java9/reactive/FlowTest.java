package com.fengjx.hello.java9.reactive;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

/**
 * @author fengjianxin
 */
public class FlowTest {
    private static final int count = 100;
    private static final CountDownLatch latch = new CountDownLatch(count);


    public static void main(String[] args) {
        SubmissionPublisher<Integer> pub = null;
        try {
            pub = new SubmissionPublisher<>();

            Flow.Subscriber<Integer> sub = new Flow.Subscriber<>() {

                private Flow.Subscription subscription;

                @Override
                public void onSubscribe(Flow.Subscription subscription) {
                    this.subscription = subscription;
                    // 控制数据处理速度
                    this.subscription.request(10);
                }

                @Override
                public void onNext(Integer item) {

                    System.out.println("onNext: " + item);
                    this.subscription.request(1);

                    // 不再接收数据
                    // this.subscription.cancel();

                    latch.countDown();
                }

                @Override
                public void onError(Throwable throwable) {
                    throwable.printStackTrace();
                    this.subscription.cancel();
                }

                @Override
                public void onComplete() {
                    System.out.println("onComplete");
                }
            };

            pub.subscribe(sub);

            for (int i = 0; i < count; i++) {
                // 当数据超过Subscriber定义的数据量，submit方法会阻塞
                pub.submit(i);
            }
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (pub != null) {
                pub.close();
            }
        }


    }


}
