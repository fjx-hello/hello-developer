package com.fengjx.hello.java.thread;

/**
 * 停止线程示例
 *
 * @author fengjianxin
 */
public class ThreadStop extends Thread {

    private volatile boolean running = true;

    public static void main(String[] args) {
        ThreadStop thread = new ThreadStop();
        thread.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread.setRunning(false);
        // 不要使用stop停止线程
        // thread.stop();
    }

    @Override
    public void run() {
        while (running) {
        }
        System.out.println("stop...");
    }

    public boolean isIsRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
