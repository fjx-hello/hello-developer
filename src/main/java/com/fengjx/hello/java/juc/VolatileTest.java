package com.fengjx.hello.java.juc;

import java.util.concurrent.TimeUnit;

/**
 * @author fengjianxin
 */
public class VolatileTest {

    private static boolean running = true;

    public static void main(String[] args) {
        SomeTask t = new SomeTask();
        t.start();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t.stopTask();

    }

    private static class SomeTask extends Thread {


        private int count = 0;

        @Override
        public void run() {
            while (running) {
                System.out.println(++count + " do something...");
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void stopTask() {
            running = false;
        }

    }
}


