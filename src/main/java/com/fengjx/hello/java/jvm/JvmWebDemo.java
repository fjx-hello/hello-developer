package com.fengjx.hello.java.jvm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * JvmWebDemo
 */
@RestController
@SpringBootApplication
public class JvmWebDemo {

    public static void main(String[] args) {
        SpringApplication.run(JvmWebDemo.class, args);
    }

    @RequestMapping(value = "/jvm-big-obj")
    public String bigObj(HttpServletRequest request) {
        List<Byte[]> temp = new ArrayList<>();
        Byte[] b = new Byte[1024 * 1024];
        temp.add(b);
        return "success";
    }


}