package com.fengjx.hello.java.java8;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Set;

/**
 * DateTest
 */
public class DateTest {

    public static void main(String[] args) {
        // 日期（时间）常用api
        LocalDateTime nowDateTime = LocalDateTime.now();
        System.out.println("当前日期和时间：" + nowDateTime);
        LocalDate nowDate = LocalDate.now();
        System.out.println("当前日期：" + nowDate);
        LocalTime nowTime = LocalTime.now();
        System.out.println("当前时间：" + nowTime + ", 不显示毫秒：" + nowTime.withNano(0));

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println("格式化当前日期和时间：" + dtf.format(nowDateTime));

        LocalDateTime date1 = LocalDateTime.parse("2000-01-01 21:24:54", dtf);
        System.out.println("date1: " + date1);

        LocalDateTime date2 = LocalDateTime.of(2000, 07, 28, 12, 30, 10);
        System.out.println("date2: " + date2);

        LocalDateTime date3 = date1.plusDays(30);
        System.out.println("date3: " + date3);

        long day = date1.until(date3, ChronoUnit.DAYS);
        System.out.println("date1与date3相差天数：" + day);
        Period period = Period.between(date1.toLocalDate(), date3.toLocalDate());
        System.out.println(
                String.format("date1与date3相差：%d年 %d月 %d日", period.getYears(), period.getMonths(), period.getDays()));

        LocalDate firstDayOfMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate lastDayOfMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
        System.out.println(String.format("当月的第一天：%s, 最后一天：%s", firstDayOfMonth, lastDayOfMonth));

        // 时区
        Set<String> zoneIds = ZoneId.getAvailableZoneIds();
        zoneIds.forEach(System.out::println); // 遍历所有时区

        ZoneId sysZone = ZoneId.systemDefault();
        System.out.println("系统时区：" + sysZone);

        ZoneId losZone = ZoneId.of("America/Los_Angeles");
        LocalDateTime losDateTime = LocalDateTime.now(losZone);
        System.out.println("当前洛杉矶时间：" + losDateTime);

        // 时间戳
        long currentTimeMillis = System.currentTimeMillis();
        System.out.println("long转Instant：" + Instant.ofEpochMilli(currentTimeMillis).toEpochMilli());

        Instant instant = Instant.now();
        System.out.println("当前时间戳：" + instant.toEpochMilli());
        Instant instant2 = nowDateTime.atZone(sysZone).toInstant(); // LocalDateTime转Instant

        long duraMillis = Duration.between(instant2, instant).toMillis();
        System.out.println("instant2与instant相差毫秒数：" + duraMillis);

        // 与Instant、Date、LocalDateTime相互转化
        LocalDateTime insToDateTime = LocalDateTime.ofInstant(instant, sysZone);
        System.out.println("Instant转LocalDateTime：" + insToDateTime);

        Date date = Date.from(instant);
        System.out.println("Instant转Date：" + date);

        Instant dateTimeToInstant = nowDateTime.atZone(sysZone).toInstant();
        System.out.println("LocalDateTime转Instant：" + dateTimeToInstant.toEpochMilli());

    }

}