package com.fengjx.hello.java.java8;

import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * LambdaFunctionTest
 */
public class LambdaFunctionTest {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService pool = Executors.newSingleThreadExecutor();
        CountDownLatch cdl = new CountDownLatch(2);

        pool.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("java8之前的写法");
                cdl.countDown();
            }
        });

        pool.submit(() -> {
            System.out.println("Lambda表达式写法");
            cdl.countDown();
        });

        cdl.await();
        pool.shutdown();

        // Lambda创建一个比较器
        Comparator<Integer> com = (o1, o2) -> o2 - o1;
        List<Integer> list1 = Arrays.asList(1, 20, 30, 13, 40);
        List<Integer> list2 = Arrays.asList(1, 20, 30, 13, 40);
        list1.sort(com);
        System.out.println(list1);

        // 省去Comparator定义
        list2.sort((o1, o2) -> o2 - o1);
        System.out.println(list2);

        Consumer<Integer> c1 = i -> {
            i = i * 2;
            System.out.println("i * 2 = " + i);
        };
        c1.accept(10);

        Supplier<String> s1 = () -> "fjx" + Instant.now().toEpochMilli();
        System.out.println(s1.get());

        Function<Long, String> f1 = l -> "fjx" + l;
        System.out.println(f1.apply(Instant.now().toEpochMilli()));

        Predicate<String> p1 = str -> str == null;
        System.out.println(p1.test("fjx"));

    }
}