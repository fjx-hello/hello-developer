package com.fengjx.hello.java.java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * MethodRef
 */
public class MethodRefTest {


    public MethodRefTest() {
    }

    public void hello(String name) {
        System.out.println("hello: " + name);
    }

    public static int len(String str) {
        return str == null ? 0 : str.length();
    }

    public static void main(String[] args) {

        // 实例方法引用
        List<String> list = Arrays.asList("a", "b", "c");
        list.forEach(System.out::println);

        // 构造器引用
        Supplier<MethodRefTest> supplier = MethodRefTest::new;
        MethodRefTest mr = supplier.get();

        // 静态方法引用
        Function<String, Integer> fun = MethodRefTest::len;
        int len = fun.apply("abc");
        System.out.println(len);

        // 实例方法引用
        Consumer<String> hello = mr::hello;
        hello.accept("fjx");

        // 构造器引用, 类方法引用, 实例方法引用
        Stream.generate(MethodRefTest::new).limit(5)
            .map(MethodRefTest::hashCode)
            .forEach(System.out::println);

    }
}