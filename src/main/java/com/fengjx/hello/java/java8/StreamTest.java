package com.fengjx.hello.java.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author fengjianxin
 * @version 2019-07-04
 */
public class StreamTest {

    public static void main(String[] args) {
        List<String> strList = Arrays.asList("a", " ", "sas", "1", "39", "as234","nba","cba", "32", "25", "ss ", " srr ", "10", "", " bqwe");

        // 查找出全部数字，按照字典排序，并将结果收集到List<Integer>
        List<Integer> intList = strList.stream()
            .filter(str -> str.trim().length() > 0)
            .filter(str -> isInteger(str))
            .map(str -> Integer.parseInt(str))
            .sorted()
            .collect(Collectors.toList());
            System.out.println(intList);

        // 查找出所有数字中最大的值是多少
        Optional<Integer> max = strList.stream()
            .filter(str -> str.trim().length() > 0)
            .filter(str -> isInteger(str))
            .map(str -> Integer.parseInt(str)).max((a, b) -> a - b);
        System.out.println("max: " + max.get());

        // 查找出所有数字的总和
        Optional<Integer> sum = strList.stream()
            .filter(str -> str.trim().length() > 0)
            .filter(str -> isInteger(str))
            .map(str -> Integer.parseInt(str))
            .reduce((a, b) -> a + b);
        System.out.println("sum: " + sum.get());

        // 查找出所有数字的个数
        long count = strList.stream()
            .filter(str -> str.trim().length() > 0)
            .filter(str -> isInteger(str))
            .map(str -> Integer.parseInt(str))
            .count();
        System.out.println("count: " + count);

        // 查找出所有数字的平均值
        Double ave = strList.stream()
            .filter(str -> str.trim().length() > 0)
            .filter(str -> isInteger(str))
            .map(str -> Integer.parseInt(str))
            .collect(Collectors.averagingInt(value -> value));
        System.out.println("ave: " + ave);

        // 去掉所有字符串两边空格，并获得所有以字母a开头的字符串
        strList.stream()
            .map(str -> str.trim())
            .filter(str -> str.startsWith("a"))
            .filter(str -> !isInteger(str))
            .forEach(System.out::println);

        // createStream();
    }

    public static boolean isInteger(String str) {  
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");  
        return pattern.matcher(str).matches();  
    }

    /**
     * 创建流
     */
    public static void createStream() {
        // list转stream
        List<String> strList = new ArrayList<>();
        Stream<String> listStream = strList.stream();

        // 数组转stream
        String[] strArr = new String[10];
        Stream<String> arrStream = Arrays.stream(strArr);

        // 通过静态方法创建流
        Stream<String> stream = Stream.of("a", "b", "c");

        // 通过种子创建无限流
        Stream<Integer> iterStream = Stream.iterate(2, i -> i * i);
        // iterStream.forEach(System.out::println);
        /* 这里将无限打印输出
         * 2
         * 4
         * 8
         * 16
         * 32
         * 64
         * 128
         * 256
         * 512
         * 1024
         * 2048
         * ...
         */

        // 创建10个元素的流
        Stream<Integer> iter10Stream = Stream.iterate(2, i -> i * 2).limit(10);
        iter10Stream.forEach(System.out::println);
        /* 输出
         * 2
         * 4
         * 8
         * 16
         * 32
         * 64
         * 128
         * 256
         * 512
         * 1024
         */

        // 生成流
        Stream<Double> genStream = Stream.generate(Math::random).limit(3);
        genStream.forEach(System.out::println);
        /* 输出
         * 0.5875071685084056
         * 0.622271393744705
         * 0.7873169115032873
         */

    }

}
