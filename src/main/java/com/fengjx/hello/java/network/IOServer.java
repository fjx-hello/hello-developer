package com.fengjx.hello.java.network;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IOServer extends Thread {

    private volatile boolean running = true;
    private ServerSocket serverSocket;
    private ExecutorService pool = Executors.newFixedThreadPool(5);

    public void stopServer() {
        this.running = false;
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        pool.shutdown();
    }


    @Override
    public void run() {
        try {
            // port=0，表示随机监听一个空闲端口
            serverSocket = new ServerSocket(0);
            System.out.println("server started, listing port: " + serverSocket.getLocalPort());
            while (running) {
                Socket socket = serverSocket.accept();
                RequestHandler handler = new RequestHandler(socket);
                pool.execute(handler);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            stopServer();
        }
    }

    public static void main(String[] args) {
        new IOServer().start();
    }

    private static class RequestHandler extends Thread {
        private volatile boolean alive = true;
        private Socket socketClient;
        private Scanner scanner;
        private PrintStream out;

        RequestHandler(Socket socketClient) {
            this.socketClient = socketClient;
            try {
                this.scanner = new Scanner(socketClient.getInputStream());
                this.scanner.useDelimiter("\n");
                this.out = new PrintStream(socketClient.getOutputStream());
                System.out.println("client connectd: " + socketClient.getInetAddress().getHostAddress());
                this.out.println("hello");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void close() {
            alive = false;
            scanner.close();
            out.close();
            if (!socketClient.isClosed()) {
                try {
                    socketClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void run() {
            while (alive) {
                if (socketClient.isClosed()) {
                    System.out.println("client closed");
                    this.close();
                    return;
                }
                if (this.scanner.hasNext()) {
                    String data = this.scanner.next().trim();
                    if ("".equals(data)) {
                        continue;
                    }
                    if ("exit".equalsIgnoreCase(data)) {
                        out.println("bye...");
                        this.close();
                    } else {
                        System.out.printf("receive: %s \n", data);
                        out.printf("receive: %s \n", data);
                    }
                }
            }
        }
    }
}
