package com.fengjx.hello.java.network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AIOServer {

    private int port;
    private ExecutorService executorService;
    private AsynchronousChannelGroup threadGroup;
    public AsynchronousServerSocketChannel asynServerSocketChannel;

    public AIOServer(int port) {
        this.port = port;
    }

    public AIOServer start() {
        try {
            executorService = Executors.newCachedThreadPool();
            threadGroup = AsynchronousChannelGroup.withCachedThreadPool(executorService, 1);
            asynServerSocketChannel = AsynchronousServerSocketChannel.open(threadGroup);
            asynServerSocketChannel.bind(new InetSocketAddress(port));
            System.out.println("server start , port : " + port);
            asynServerSocketChannel.accept(this, new AcceptHandler());
            return this;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void sync() {
        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class AcceptHandler implements CompletionHandler<AsynchronousSocketChannel, AIOServer> {

        @Override
        public void completed(AsynchronousSocketChannel clientChannel, AIOServer attachment) {
            attachment.asynServerSocketChannel.accept(attachment, this);
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            clientChannel.read(buffer, buffer, new LogicHandler(clientChannel));
        }

        @Override
        public void failed(Throwable exc, AIOServer attachment) {
            exc.printStackTrace();
        }

    }

    static class LogicHandler implements CompletionHandler<Integer, ByteBuffer> {

        private AsynchronousSocketChannel clientChannel;

        public LogicHandler(AsynchronousSocketChannel clientChannel) {
            this.clientChannel = clientChannel;
        }

        @Override
        public void completed(Integer result, ByteBuffer attachment) {
            System.out.println("result: " + result);
            if (!clientChannel.isOpen() || result < 0) {
                System.out.println("client close");
                this.close();
                return;
            }
            attachment.flip();
            String data = new String(attachment.array()).trim();
            if ("exit".equalsIgnoreCase(data)) {
                this.close();
            }
            write(clientChannel, "receive: " + data + "\r\n");
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            clientChannel.read(buffer, buffer, this);
        }

        @Override
        public void failed(Throwable exc, ByteBuffer attachment) {
            exc.printStackTrace();
        }

        private void write(AsynchronousSocketChannel clientChannel, String response) {
            try {
                ByteBuffer buf = ByteBuffer.allocate(1024);
                buf.put(response.getBytes());
                buf.flip();
                clientChannel.write(buf).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        private void close() {
            try {
                clientChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        AIOServer server = new AIOServer(6666);
        server.start().sync();
    }
}
