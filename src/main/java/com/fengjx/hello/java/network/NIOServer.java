package com.fengjx.hello.java.network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;

public class NIOServer extends Thread {

    private int port;
    private ServerSocketChannel serverChannel;
    private Selector selector;
    private ByteBuffer readBuffer = ByteBuffer.allocate(1024);

    private Map<SocketChannel, Queue<ByteBuffer>> pendingData = new ConcurrentHashMap<>(10);

    public NIOServer(int port) throws IOException {
        this.port = port;
        this.selector = this.initSelector();
    }

    private Selector initSelector() throws IOException {
        Selector socketSelector = SelectorProvider.provider().openSelector();
        this.serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        InetSocketAddress isa = new InetSocketAddress(this.port);
        serverChannel.socket().bind(isa);
        serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);
        System.out.println("server started, listing port: " + port);
        return socketSelector;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (this.selector.select() < 1) {
                    continue;
                }
                Iterator selectedKeys = this.selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = (SelectionKey) selectedKeys.next();
                    selectedKeys.remove();
                    if (!key.isValid()) {
                        continue;
                    }
                    if (key.isAcceptable()) {
                        this.accept(key);
                    } else if (key.isReadable()) {
                        this.read(key);
                    } else if (key.isWritable()) {
                        this.write(key);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);
        // 响应数据消息队列
        Queue<ByteBuffer> queue = new LinkedBlockingDeque<>(10);
        pendingData.put(socketChannel, queue);
        socketChannel.register(this.selector, SelectionKey.OP_READ);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        this.readBuffer.clear();
        int numRead;
        try {
            numRead = socketChannel.read(this.readBuffer);
        } catch (IOException e) {
            closeClient(key);
            return;
        }

        if (numRead == -1) {
            closeClient(key);
            return;
        }
        String data = new String(readBuffer.array(), 0, numRead).trim();
        // 退出指令
        if ("exit".equalsIgnoreCase(data)) {
            closeClient(key);
            return;
        }
        pendingData.get(socketChannel).add(Charset.defaultCharset().encode("receive: " + data + "\n"));
        key.interestOps(SelectionKey.OP_WRITE);
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        Queue<ByteBuffer> queue = pendingData.get(socketChannel);
        if (queue.isEmpty()) {
            return;
        }
        ByteBuffer buffer = queue.poll();
        while (buffer != null) {
            socketChannel.write(buffer);
            buffer = queue.poll();
        }

        if (queue.isEmpty()) {
            key.interestOps(SelectionKey.OP_READ);
        }
    }

    private void closeClient(SelectionKey key) throws IOException {
        key.channel().close();
        key.cancel();
    }


    public static void main(String[] args) {
        try {
            new NIOServer(6666).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
