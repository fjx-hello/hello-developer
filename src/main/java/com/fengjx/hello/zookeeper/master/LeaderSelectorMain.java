package com.fengjx.hello.zookeeper.master;

import com.google.common.collect.Lists;

import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * 服务选举测试
 */
public class LeaderSelectorMain {
    private static final int CLIENT_QTY = 10;

    private static final String PATH = "/examples/leader";

    public static void main(String[] args) throws Exception {
        List<CuratorFramework> clients = Lists.newArrayList();
        List<WorkServer> workServers = Lists.newArrayList();
        try {
            for (int i = 0; i < CLIENT_QTY; ++i) {
                CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1:2181",
                        new ExponentialBackoffRetry(1000, 3));
                clients.add(client);
                WorkServer workServer = new WorkServer(client, PATH, i);
                workServers.add(workServer);
                client.start();
                workServer.start();
            }

            System.out.println("回车键退出\n");
            new BufferedReader(new InputStreamReader(System.in)).readLine();
        } finally {
            System.out.println("Shutting down...");
            for (WorkServer workServer : workServers) {
                CloseableUtils.closeQuietly(workServer);
            }
            for (CuratorFramework client : clients) {
                CloseableUtils.closeQuietly(client);
            }
        }
    }
}
