package com.fengjx.hello.zookeeper.master;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListenerAdapter;

import java.io.Closeable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * worker服务
 */
public class WorkServer extends LeaderSelectorListenerAdapter implements Closeable {

    private final ServerNode node;
    private final LeaderSelector leaderSelector;
    private final AtomicInteger leaderCount = new AtomicInteger();

    public WorkServer(CuratorFramework client, String path, int id) {
        this.node = new ServerNode(id);
        // 创建leader选举对象
        leaderSelector = new LeaderSelector(client, path, this);
        leaderSelector.setId(id + "");
        // 在释放leader权限后自动重新参加选举
        leaderSelector.autoRequeue();
    }

    public void start() {
        System.out.println(this.node.getName() + " start...");
        // 服务启动
        leaderSelector.start();
    }

    @Override
    public void close() {
        System.out.println(this.node.getName() + " close...");
        // 关闭服务
        leaderSelector.close();
    }

    /**
     * 争抢到leader权限后的回调方法
     */
    @Override
    public void takeLeadership(CuratorFramework client) {
        this.node.setActive(true);
        String name = this.node.getName();
        try {
            final int waitSeconds = (int) (5 * Math.random()) + 1;
            System.out.println(name + " is now the leader. Waiting " + waitSeconds + " seconds...");
            System.out.println(name + " has been leader " + leaderCount.getAndIncrement() + " time(s) before.");
            Thread.sleep(TimeUnit.SECONDS.toMillis(waitSeconds));
        } catch (InterruptedException e) {
            System.err.println(name + " was interrupted.");
            Thread.currentThread().interrupt();
        } finally {
            // 执行完毕，释放leader权限
            System.out.println(name + " relinquishing leadership.\n");
        }
    }
}
