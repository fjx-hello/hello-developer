package com.fengjx.hello.zookeeper.master;

import java.io.Serializable;

/**
 * 服务节点信息
 */
public class ServerNode implements Serializable {

    private static final long serialVersionUID = -522933808813265389L;

    private int id;
    private String name;
    private boolean active = false;

    public ServerNode(int id) {
        this.id = id;
        this.name = "Client #" + id;
    }

    public ServerNode(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "ServerNode{" + "id=" + id + ", name='" + name + '\'' + ", active=" + active + '}';
    }
}
