package com.fengjx.hello.zookeeper.discovery;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.apache.curator.x.discovery.ServiceDiscoveryBuilder;
import org.apache.curator.x.discovery.ServiceInstance;
import org.apache.curator.x.discovery.UriSpec;
import org.apache.curator.x.discovery.details.JsonInstanceSerializer;

import java.io.Closeable;

public class ExampleServer implements Closeable {
    private final ServiceDiscovery<InstanceDetails> serviceDiscovery;
    private final ServiceInstance<InstanceDetails> thisInstance;

    public ExampleServer(CuratorFramework client, String path, String serviceName, String description)
            throws Exception {
        UriSpec uriSpec = new UriSpec("{scheme}://foo.com:{port}");
        // 构造服务信息
        thisInstance = ServiceInstance.<InstanceDetails> builder().name(serviceName)
                .payload(new InstanceDetails(description)).port((int) (65535 * Math.random())).uriSpec(uriSpec).build();
        // 服务信息json序列化器
        JsonInstanceSerializer<InstanceDetails> serializer = new JsonInstanceSerializer<InstanceDetails>(
                InstanceDetails.class);
        // 创建服务发现对象（用户注册，查询，修改，删除服务）
        serviceDiscovery = ServiceDiscoveryBuilder.builder(InstanceDetails.class).client(client).basePath(path)
                .serializer(serializer).thisInstance(thisInstance).build();
    }

    public ServiceInstance<InstanceDetails> getThisInstance() {
        return thisInstance;
    }

    public void start() throws Exception {
        // 服务启动，将节点信息写入zookeeper
        serviceDiscovery.start();
    }

    @Override
    public void close() {
        // 服务关闭，将节点数据删除，并断开与zookeeper连接
        CloseableUtils.closeQuietly(serviceDiscovery);
    }
}
