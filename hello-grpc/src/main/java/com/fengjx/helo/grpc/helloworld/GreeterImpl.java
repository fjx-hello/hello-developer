package com.fengjx.helo.grpc.helloworld;

import com.fengjx.hello.grpc.helloworld.GreeterGrpc;
import com.fengjx.hello.grpc.helloworld.HelloReply;
import com.fengjx.hello.grpc.helloworld.HelloRequest;
import io.grpc.stub.StreamObserver;

/**
 * @author fengjianxin
 */
public class GreeterImpl extends GreeterGrpc.GreeterImplBase {

    @Override
    public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
        HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + req.getName()).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

    @Override
    public void ping(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
        HelloReply reply = HelloReply.newBuilder().setMessage("pong from: " + req.getName()).build();
        responseObserver.onNext(reply);
        responseObserver.onNext(reply);
        responseObserver.onNext(reply);
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
