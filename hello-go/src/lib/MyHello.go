package lib

import "fmt"

func init() {
	fmt.Println("MyHello init1")
}

func init() {
	fmt.Println("MyHello init2")
}

// 大写开头的方法可以被包外访问
func Hello(name string) {
	fmt.Println("Hello", name)
}

// 小写开头，只能包内调用
func hello(name string) {
	fmt.Println("hello", name)
}
