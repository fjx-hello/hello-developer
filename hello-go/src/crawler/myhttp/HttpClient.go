package myhttp

import (
	"errors"
	"fmt"
	"hello-go/src/conf"
	"hello-go/src/crawler/model"
	"hello-go/src/crawler/utils"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var proxyClient http.Client
var httpClient http.Client

func init() {
	proxyURL, err := url.Parse(conf.String("crawler.http.proxy"))
	if err != nil {
		log.Println(err)
	}

	//adding the proxy settings to the Transport object
	transport := &http.Transport{
		Proxy: http.ProxyURL(proxyURL),
	}

	cookieJar, _ := cookiejar.New(nil)
	//adding the Transport object to the http Client
	proxyClient = http.Client{
		Transport: transport,
		Jar:       cookieJar,
		Timeout:   time.Duration(time.Second * 20),
	}

	httpClient = http.Client{
		Timeout: time.Duration(time.Second * 5),
	}
	log.Println("http client init")
}

func ProxyBody(urlStr string, referer string) (*http.Response, error) {
	reqUrl, err := url.Parse(urlStr)
	if err != nil {
		log.Println("Parse url", err)
	}
	request, err := http.NewRequest("GET", reqUrl.String(), nil)
	if err != nil {
		log.Println("create request error", urlStr, err)
		return nil, err
	}
	request.Header.Add("User-Agent", conf.String("crawler.http.ua"))
	if referer != "" {
		request.Header.Add("Referer", referer)
	}

	//calling the URL
	resp, err := proxyClient.Do(request)
	if err != nil {
		log.Println("do request error", urlStr, err)
		return nil, err
	}

	return resp, nil
}

func DoBody(urlStr string, referer string) (*http.Response, error) {
	reqUrl, err := url.Parse(urlStr)
	if err != nil {
		log.Println("Parse url", err)
	}
	request, err := http.NewRequest("GET", reqUrl.String(), nil)
	if err != nil {
		log.Println("create request error", urlStr, err)
		return nil, err
	}
	request.Header.Add("User-Agent", conf.String("crawler.http.ua"))
	if referer != "" {
		request.Header.Add("Referer", referer)
	}

	//calling the URL
	resp, err := httpClient.Do(request)
	if err != nil {
		log.Println("do request error", urlStr, err)
		return nil, err
	}

	return resp, nil
}

func DownloadImage(imageItem model.ImageItem, useProxy bool) error {

	_, fileName := filepath.Split(imageItem.ImgUrl)
	baseDir := conf.String("crawler.download.baseDir")
	path := strings.Join([]string{baseDir, imageItem.ImgType, imageItem.Dir}, "/")
	filePath := strings.Join([]string{path, fileName}, "/")

	if utils.IsFileOrDirExist(filePath) {
		//log.Println("exist", filePath, imgUrl)
		//return nil
		if err := utils.IsImage(filePath); err != nil {
			log.Println("not a image file", filePath)
			utils.RemoveFile(filePath)
		} else {
			log.Println("exist", filePath, imageItem.ImgUrl)
			return nil
		}

	}

	var resp *http.Response
	var err error
	if useProxy {
		log.Println("download use proxy", imageItem.ImgUrl)
		resp, err = ProxyBody(imageItem.ImgUrl, imageItem.Referer)
	} else {
		resp, err = DoBody(imageItem.ImgUrl, imageItem.Referer)
	}

	if err != nil {
		log.Println("download Get error", imageItem.ImgUrl, err)
		return err
	}

	defer utils.Close(resp.Body)

	utils.CreateDirIfNotExist(path)
	file, err := os.Create(filePath)
	if err != nil {
		log.Println("create file", err)
		utils.RemoveFile(filePath)
		return err
	}
	defer utils.Close(file)

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		log.Println("io copy error", err)
		utils.RemoveFile(filePath)
		return err
	}
	log.Println("download success", filePath)
	return nil
}

func DownloadImageRetry(imageItem model.ImageItem, retry int) error {
	for i := 1; i <= retry; i++ {
		err := DownloadImage(imageItem, i > 2)
		if err == nil {
			return nil
		}
		log.Println("download error and retry", i, imageItem.ImgUrl)
	}
	return errors.New(fmt.Sprintf("download image error: %s, %s, %s", imageItem))
}
