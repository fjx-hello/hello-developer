package main

import (
	"flag"
	"hello-go/src/conf"
	"hello-go/src/crawler/model"
	"hello-go/src/crawler/mycrawler"
	"hello-go/src/crawler/parser"
	"log"
)

func main() {

	start := flag.Int("start", 1, "crawler start page")
	end := flag.Int("end", 1, "crawler end page")

	var chPage = make(chan model.PageItem, 1)
	var chDownload = make(chan model.ImageItem, 50)
	var chDownloadFail = make(chan model.ImageItem, 10)

	config, err := conf.GetConfig().Get("crawler.meitu")
	if err != nil {
		log.Fatal("get config error", err)
	}
	// var t66yParser parser.Parser = parser.T66yParser{Config: config}
	crawler := mycrawler.MyCrawler{
		ChPage:         chPage,
		ChDownload:     chDownload,
		ChDownloadFail: chDownloadFail,
		Parser:         &parser.MeituParser{Config: config},
	}
	log.Println("crawler", *start, *end)
	crawler.Start(*start, *end)
}
