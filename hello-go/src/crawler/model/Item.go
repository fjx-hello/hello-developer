package model

type ImageItem struct {
	ImgUrl  string
	ImgType string
	Dir     string
	Referer string
}

type PageItem struct {
	PageUrl string
	ImgType string
	Dir     string
}
