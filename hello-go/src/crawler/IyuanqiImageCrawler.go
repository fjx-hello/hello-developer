package main

import (
	"fmt"
	"hello-go/src/conf"
	"hello-go/src/crawler/model"
	"hello-go/src/crawler/mycrawler"
	"hello-go/src/crawler/parser"
	"log"
)

func main() {

	var chPage = make(chan model.PageItem, 2)
	var chDownload = make(chan model.ImageItem, 30)
	var chDownloadFail = make(chan model.ImageItem, 20)

	config, err := conf.GetConfig().Get("crawler.iyuanqi")
	if err != nil {
		log.Fatal("get config error", err)
	}
	crawler := mycrawler.MyCrawler{
		ChPage:         chPage,
		ChDownload:     chDownload,
		ChDownloadFail: chDownloadFail,
		Parser:         &parser.IyuanqiParser{Config: config},
	}
	crawler.Start(1, 5)
	fmt.Println("crawler end, press any key to exit...")
	// 任意键退出
	fmt.Println(fmt.Scanln())
}
