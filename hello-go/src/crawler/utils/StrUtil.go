package utils

import "github.com/axgle/mahonia"

var enc = mahonia.NewDecoder("gbk")

// gbk转utf8
func GbkToUtf8(str string) string {
	return enc.ConvertString(str)
}
