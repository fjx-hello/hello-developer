package utils

import (
	"image"
	_ "image/jpeg"
	_ "image/gif"
	_ "image/png"
	_ "image/jpeg"
	"os"
)

func IsImage(filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}

	defer Close(file)
	_, _, err = image.Decode(file)

	if err != nil {
		return err
	}
	return nil
}
