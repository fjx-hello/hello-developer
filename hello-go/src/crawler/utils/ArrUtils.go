package utils

func StringsContains(array []string, val string) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return index
		}
	}
	return index
}

func StringsInterfaceContains(array []interface{}, val string) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i].(string) == val {
			index = i
			return index
		}
	}
	return index
}
