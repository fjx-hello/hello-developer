package utils

import (
	"io"
	"log"
)

func Close(c io.ReadCloser) {
	err := c.Close()
	if err != nil {
		log.Panicln("close error", c)
	}
}
