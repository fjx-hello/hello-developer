package parser

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/olebedev/config"
	"hello-go/src/crawler/model"
	"hello-go/src/crawler/myhttp"
	"hello-go/src/crawler/utils"
	"log"
	"strconv"
	"strings"
)

type T66yParser struct {
	Config *config.Config
}

func (parser *T66yParser) ParserImage(page model.PageItem) ([]model.ImageItem, error) {
	log.Println("ParserImage", page)
	resp, err := myhttp.ProxyBody(page.PageUrl, "")
	if err != nil {
		log.Println("request image page error", err)
		return nil, err
	}

	defer utils.Close(resp.Body)

	if resp.StatusCode != 200 {
		log.Printf("status code error: %d %s \n", resp.StatusCode, resp.Status)
		return nil, HttpError

	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)

	if err != nil {
		log.Println("parserImage NewDocumentFromReader error", err)
		return nil, err
	}

	res := make([]model.ImageItem, 0, 20)
	// Find the review items
	doc.Find(".tpc_content > input").Each(func(i int, s *goquery.Selection) {
		imageURL, _ := s.Attr("data-src")
		if imageURL != "" {
			res = append(res, model.ImageItem{ImgUrl: imageURL, Dir: page.Dir, ImgType: page.ImgType})
		}
	})
	return res, nil
}

func (parser *T66yParser) ParserPage(page int, chPage chan model.PageItem) {
	pageListUrl, _ := parser.Config.String("thread0806_8")
	url := pageListUrl + strconv.Itoa(page)
	log.Println("parserPage", url)

	resp, err := myhttp.ProxyBody(url, "")
	if err != nil {
		log.Println("request page error ", err)
		return
	}

	defer utils.Close(resp.Body)

	if resp.StatusCode != 200 {
		log.Printf("parserPage status code error: %d %s \n", resp.StatusCode, resp.Status)
		return
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Println("parserPage NewDocumentFromReader error", err)
		return
	}

	doc.Find(".tr3").Each(func(i int, sel *goquery.Selection) {
		linkSelection := sel.Find(".tal")
		dataSelection := sel.Find(".f10")

		if page == 1 && i <= 6 {
			return
		}
		text := utils.GbkToUtf8(linkSelection.Text())
		start := strings.Index(text, "[") + 1
		end := strings.Index(text, "]")
		imgType := string([]byte(text)[start:end])

		types, _ := parser.Config.List("targetTypes")
		if utils.StringsInterfaceContains(types, imgType) == -1 {
			log.Println("skip type:", imgType)
			return
		}
		date := strings.Split(strings.TrimSpace(dataSelection.Text()), " ")[0]
		imgType = strings.Join([]string{"t66y", imgType, date}, "/")
		a := linkSelection.Find("h3 > a")
		href, ok := a.Attr("href")

		dir := utils.GbkToUtf8(a.Text())

		if ok {
			baseUrl, _ := parser.Config.String("baseUrl")
			chPage <- model.PageItem{ImgType: imgType, PageUrl: baseUrl + href, Dir: dir}
		} else {
			log.Println("parserPage href is nil")
		}
	})
}
