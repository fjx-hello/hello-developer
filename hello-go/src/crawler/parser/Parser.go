package parser

import (
	"errors"
	"hello-go/src/crawler/model"
)

var HttpError = errors.New("http error")

type Parser interface {
	ParserImage(page model.PageItem, ) ([]model.ImageItem, error)
	ParserPage(page int, chPage chan model.PageItem)
}
