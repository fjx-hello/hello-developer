package parser

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/olebedev/config"
	"hello-go/src/crawler/model"
	"hello-go/src/crawler/myhttp"
	"hello-go/src/crawler/utils"
	"log"
	"strings"
)

type Xgmn5Parser struct {
	Config *config.Config
}

func (parser *Xgmn5Parser) ParserImage(page model.PageItem) ([]model.ImageItem, error) {
	log.Println("ParserImage", page)
	resp, err := myhttp.DoBody(page.PageUrl, "")
	if err != nil {
		log.Println("request image page error", err)
		return nil, err
	}

	defer utils.Close(resp.Body)

	if resp.StatusCode != 200 {
		log.Printf("status code error: %d %s \n", resp.StatusCode, resp.Status)
		return nil, HttpError

	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)

	if err != nil {
		log.Println("parserImage NewDocumentFromReader error", err)
		return nil, err
	}

	weizhi := doc.Find(".weizhi").Find("a").Eq(1).Text()
	imgType := strings.Join([]string{page.ImgType, weizhi}, "/")
	res := make([]model.ImageItem, 0, 20)
	// Find the review items
	doc.Find(".content_img").Each(func(i int, s *goquery.Selection) {
		imageURL, _ := s.Attr("src")
		if imageURL != "" {
			res = append(res, model.ImageItem{ImgUrl: imageURL, Dir: page.Dir, ImgType: imgType, Referer: page.PageUrl})
		}

	})

	span := doc.Find("#pages").Find("span")
	next := span.Next()
	hasNext := !next.HasClass("a1")
	if hasNext {
		baseUrl, _ := parser.Config.String("baseUrl")
		href, _ := next.Attr("href")
		page.PageUrl = baseUrl + href
		items, err := parser.ParserImage(page)
		if err != nil {
			log.Println("append next image page error", err)
		} else {
			for _, val := range items {
				res = append(res, val)
			}
		}
	}
	return res, nil
}

func (parser *Xgmn5Parser) ParserPage(page int, chPage chan model.PageItem) {
	pageListUrl, err := parser.Config.List("listUrl")
	if err != nil {
		log.Println("get listUrl error", err)
		return
	}
	for idx, url := range pageListUrl {
		parser.parserPage(url.(string), idx, chPage)
	}
}

func (parser *Xgmn5Parser) parserPage(url string, page int, chPage chan model.PageItem) {
	log.Println("parserPage", url, page)

	resp, err := myhttp.DoBody(url, "")
	if err != nil {
		log.Println("request page error ", err)
		return
	}

	defer utils.Close(resp.Body)

	if resp.StatusCode != 200 {
		log.Printf("parserPage status code error: %d %s \n", resp.StatusCode, resp.Status)
		return
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Println("parserPage NewDocumentFromReader error", err)
		return
	}

	imageType := doc.Find("#tmp_downloadhelper_iframe").Prev().Text()

	doc.Find(".biank1").Each(func(i int, sel *goquery.Selection) {
		a := sel.Find("a")
		img := sel.Find("img")

		imgType := strings.Join([]string{"xgmm5", imageType}, "/")
		href, ok := a.Attr("href")
		dir, _ := img.Attr("title")

		if ok {
			chPage <- model.PageItem{ImgType: imgType, PageUrl: href, Dir: dir}
		} else {
			log.Println("parserPage href is nil")
		}

	})
}
