package parser

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/olebedev/config"
	"hello-go/src/crawler/model"
	"hello-go/src/crawler/myhttp"
	"hello-go/src/crawler/utils"
	"log"
)

type IyuanqiParser struct {
	Config *config.Config
}

func (parser *IyuanqiParser) ParserImage(page model.PageItem) ([]model.ImageItem, error) {
	log.Println("ParserImage", page)
	resp, err := myhttp.DoBody(page.PageUrl, "")
	if err != nil {
		log.Println("request page error", err)
		return nil, err
	}

	defer utils.Close(resp.Body)

	if resp.StatusCode != 200 {
		log.Printf("status code error: %d %s \n", resp.StatusCode, resp.Status)
		return nil, HttpError

	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Println("parserImage NewDocumentFromReader error", err)
		return nil, err
	}

	res := make([]model.ImageItem, 0, 20)

	doc.Find(".content > p > img").Each(func(i int, selection *goquery.Selection) {
		imageURL, _ := selection.Attr("src")
		if imageURL != "" {
			log.Println("ParserImage imageURL", imageURL)
			res = append(res, model.ImageItem{ImgUrl: imageURL, Dir: page.Dir, ImgType: page.ImgType})
		}
	})
	return res, nil
}

func (parser *IyuanqiParser) ParserPage(page int) ([]model.PageItem, error) {
	pageListUrl, _ := parser.Config.String("listUrl")

	url := fmt.Sprintf(pageListUrl, page)
	log.Println("parserPage", url)

	resp, err := myhttp.DoBody(url, "")
	if err != nil {
		log.Fatal("request page error", err)
		return nil, err
	}

	defer utils.Close(resp.Body)
	if resp.StatusCode != 200 {
		log.Printf("parserPage status code error: %d %s \n", resp.StatusCode, resp.Status)
		return nil, HttpError
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Println("parserPage NewDocumentFromReader error", err)
		return nil, err
	}

	res := make([]model.PageItem, 0, 30)

	doc.Find(".block-box > a").Each(func(i int, selection *goquery.Selection) {
		imgType := "iyuanqi/福利趣图"
		if selection.HasClass("block-img") {
			return
		}
		href, ok := selection.Attr("href")

		dir, _ := selection.Attr("title")

		if ok {
			baseUrl, _ := parser.Config.String("baseUrl")
			res = append(res, model.PageItem{ImgType: imgType, PageUrl: baseUrl + href, Dir: dir})
		} else {
			log.Println("parserPage href is nil")
		}
	})
	return res, nil
}
