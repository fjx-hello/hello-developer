package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/axgle/mahonia"
	"hello-go/src/crawler/myhttp"
	"hello-go/src/crawler/utils"
	"log"
)

func main() {
	parse4()

}

func parse1() {
	url := "http://t66y.com/htm_mob/8/1905/3527546.html"
	res, err := myhttp.ProxyBody(url)
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Find the review items
	doc.Find(".tpc_cont > input").Each(func(i int, s *goquery.Selection) {
		imageURL, _ := s.Attr("data-src")
		fmt.Printf("Review %d: %s \n", i, imageURL)
	})
}

func parse2() {
	url := "http://t66y.com/htm_data/8/1905/3529816.html"
	resp, err := myhttp.ProxyBody(url)
	if err != nil {
		log.Fatal("request page error", err)
	}

	defer utils.Close(resp.Body)
	if resp.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", resp.StatusCode, resp.Status)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	enc := mahonia.NewDecoder("gbk")
	title := enc.ConvertString(doc.Find("h4").Text())

	// Find the review items
	doc.Find(".tpc_content > input").Each(func(i int, s *goquery.Selection) {
		imageURL, _ := s.Attr("data-src")
		fmt.Println(title, i, imageURL)
	})
}

func parse3() {
	url := "http://t66y.com/htm_data/8/1905/3529816.html"
	resp, err := myhttp.ProxyBody(url)
	if err != nil {
		log.Fatal("request page error", err)
	}

	defer utils.Close(resp.Body)
	if resp.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", resp.StatusCode, resp.Status)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	enc := mahonia.NewDecoder("gbk")
	title := enc.ConvertString(doc.Find("h4").Text())

	// Find the review items
	doc.Find(".tpc_content > input").Each(func(i int, s *goquery.Selection) {
		imageURL, _ := s.Attr("data-src")
		fmt.Println("parser image", title, i, imageURL)
	})
}

func parse4() {
	url := "http://www.iyuanqi.com/f/27504"
	resp, err := myhttp.DoBody(url)
	if err != nil {
		log.Fatal("request page error", err)
	}

	defer utils.Close(resp.Body)

	//buf, _ := ioutil.ReadAll(resp.Body)
	//	//fmt.Println(string(buf))

	if resp.StatusCode != 200 {
		log.Fatalf("status code error: %d %s \n", resp.StatusCode, resp.Status)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Println("parserImage NewDocumentFromReader error", err)
	}

	res := make([]string, 0, 20)

	doc.Find(".content > p > img").Each(func(i int, selection *goquery.Selection) {
		imageURL, _ := selection.Attr("src")
		if imageURL != "" {
			log.Println("ParserImage imageURL", imageURL)
			res = append(res, imageURL)
		}
	})

	log.Println(len(res))
}
