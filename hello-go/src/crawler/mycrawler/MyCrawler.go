package mycrawler

import (
	"encoding/json"
	"hello-go/src/conf"
	"hello-go/src/crawler/model"
	"hello-go/src/crawler/myhttp"
	"hello-go/src/crawler/parser"
	"hello-go/src/crawler/utils"
	"log"
	"os"
	"sync"
	"syscall"
	"time"
)

type MyCrawler struct {
	ChPage         chan model.PageItem
	ChDownload     chan model.ImageItem
	ChDownloadFail chan model.ImageItem
	Parser         parser.Parser
}

func (crawler *MyCrawler) downloader() {
	go func(ch chan model.ImageItem) {
		for {
			if item, ok := <-ch; ok {
				if item.ImgUrl == "" {
					log.Println("ImgUrl is empty", item)
					return
				}
				err := myhttp.DownloadImageRetry(item, 5)
				if err != nil {
					log.Println("download image error", item)
					crawler.ChDownloadFail <- item
				}
			}
		}
	}(crawler.ChDownload)
}

func (crawler *MyCrawler) downloadFail() {
	go func(ch chan model.ImageItem) {
		for {
			if imageItem, ok := <-ch; ok {
				downloadFailLog(imageItem)
			}
		}
	}(crawler.ChDownloadFail)
}

func downloadFailLog(item model.ImageItem) {
	logName := conf.String("crawler.t66y.downloadErrLog")
	utils.CreateFileIfNotExist(logName)
	logFile, err := os.OpenFile(logName, syscall.O_RDWR, 0755)
	if err != nil {
		log.Println(err)
		return
	}
	buf, _ := json.Marshal(item)
	_, err = logFile.WriteString(string(buf) + "\n")
	if err != nil {
		log.Println("write download error log error", err)
		return
	}

}

func (crawler *MyCrawler) parserImage() {
	go func(ch chan model.PageItem) {
		for {
			if downLen := len(crawler.ChDownload); downLen > 100 {
				log.Println("too many download task", downLen)
				time.Sleep(time.Second * 1)
				continue
			}
			if item, ok := <-ch; ok {
				images, err := crawler.Parser.ParserImage(item)
				if err == nil {
					for _, image := range images {
						if image.ImgUrl == "" {
							continue
						}
						crawler.ChDownload <- image
					}
				} else {
					log.Println("ParserImage error", err)
				}
			}
		}
	}(crawler.ChPage)
}

func (crawler *MyCrawler) parserPage(page int) {
	go func() {
		crawler.Parser.ParserPage(page, crawler.ChPage)
	}()
}

func (crawler *MyCrawler) Start(start int, end int) {
	for i := start; i <= end; i++ {
		crawler.parserPage(i)
	}
	crawler.parserImage()
	// 同时下载线程数
	for i := 0; i < 50; i++ {
		crawler.downloader()
	}
	crawler.downloadFail()
	syncMain()
}

func syncMain() {
	wa := sync.WaitGroup{}
	wa.Add(1)
	wa.Wait()

}
