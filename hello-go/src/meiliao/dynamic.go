package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"hello-go/src/utils"
)

type UserDynamic struct {
	Id    int32
	Uid   int32
	Photo string
	Video string
	Voice string
}

const (
	serverKey   = "ffda3272-fds2-432k-482e-366d3425a942"
	//yimg_bucket = "yimg"
	// ypvt_bucket = "ypvt"
	ymd_bucket  = "ymd"
)

func main() {

	db, _ := gorm.Open("mysql", "fjx:1234@/meiliao?charset=utf8&parseTime=True&loc=Local")
	defer db.Close()

	var dys []UserDynamic
	db.Table("user_dynamic").Where("video is not null").Select("id, uid, photo, video, voice").Find(&dys)

	for _, dy := range dys {
		fmt.Println(getVurl(dy.Uid, dy.Video))
	}

}

func getPath(uid int32) string {
	keyPath := fmt.Sprintf("%s/%d/", utils.Md5(fmt.Sprintf("%d%s", uid, serverKey)), uid)
	oneLayer := uid % 100
	uid = uid / 100
	secondLayer := uid % 100
	return fmt.Sprintf("%d/%d/%s", oneLayer, secondLayer, keyPath)
}

func getNOSUrl(bucket string) string {
	return fmt.Sprintf("https://%s.nosdn.127.net/", bucket)
}

func getDynamicNOSKey(uid int32, name string) string {
	return fmt.Sprintf("%sdynamic/%s", getPath(uid), name)
}

func getVurl(uid int32, video string) string {
	return fmt.Sprintf("%s%s", getNOSUrl(ymd_bucket), getDynamicNOSKey(uid, video))
}

/*
private static String getPhotoPath(Long uid) {
	String path = "";
	String keyPath = MD5.make(uid + serverKey) + "/" + uid + "/";
	int oneLayer = (uid.intValue() % 100);
	uid = uid / 100;
	int secondLayer = uid.intValue() % 100;
	path += oneLayer + "/" + secondLayer + "/" + keyPath;
	return path;
}
*/
