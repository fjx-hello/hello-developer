package main

import (
	"fmt"
	"hello-go/src/conf"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	proxy()
}

func doGet() {
	// resp, err := http.Get("https://baidu.com")
	resp, err := http.Get("http://localhost:8080")
	if err != nil {
		fmt.Println("request error", err)
	}

	defer func() {
		err := resp.Body.Close()
		if err != nil {
			fmt.Println("close body error", err)
		}
	}()

	fmt.Println(resp.Status)
	buf, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(buf))
}

func proxy() {
	request, err := http.NewRequest("GET", "http://localhost:8080", nil)
	if err != nil {
		log.Fatal(err)
	}
	request.Header.Add("User-Agent", conf.String("crawler.http.ua"))

	//calling the URL
	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(resp.Status)
	buf, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(buf))
}
