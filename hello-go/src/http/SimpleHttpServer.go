package main

import (
	"fmt"
	"log"
	"net/http"
)

func index(writer http.ResponseWriter, req *http.Request) {
	fmt.Println(req.Header)
	_, err := writer.Write([]byte("hello go http"))
	if err != nil {
		fmt.Println("write error", err)
	}
}

func hello(writer http.ResponseWriter, req *http.Request) {
	name := req.URL.Query().Get("name")
	_, err := writer.Write([]byte("hello:" + name))
	if err != nil {
		fmt.Println("write error", err)
	}
}

func main() {

	http.HandleFunc("/", index)
	http.HandleFunc("/hello", hello)

	fmt.Println("http server start: ", 8080)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("http server start error", err)
	}

}
