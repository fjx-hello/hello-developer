package test

import (
	"fmt"
	"testing"
)

const name = "fengjianxin"
const pi float32 = 3.1415926

func TestSyntax(t *testing.T) {
	fmt.Println("hello", name)
	fmt.Println("pi =", pi)
	fmt.Println("hello go")
	fmt.Println("7.0 / 3.0 =", 7.0/3.0)

	a := 123
	fmt.Println("a =", a)

	var b string = "abc"
	fmt.Println(b)

	var d, e, f int = 1, 2, 3
	fmt.Printf("d = %d, e = %d, f = %d", d, e, f)

	for i := 1; i <= 3; i++ {
		fmt.Println("for i=", i)
	}

	for a < 150 {
		if a%2 == 0 {
			fmt.Println("%2 == 0 | ", a)
		}
		a = a + 1
	}

	/* 数组 */
	var arr [10]int
	fmt.Println(arr)
	arr[0] = 100
	arr[1] = 12
	arr[2] = 34
	arr[5] = 2345
	fmt.Println(arr)
	fmt.Println(len(arr))
	// 1 ~ 3
	fmt.Println(arr[1:4])
	// 0 ~ 3
	fmt.Println(arr[:4])
	// 1 ~ 9
	fmt.Println(arr[1:])

	arr_b := [5]int{10, 20, 30, 40, 50}
	fmt.Println(arr_b)

	for i, item := range arr_b {
		fmt.Println("range ", i, item)
	}

	// map
	m := make(map[string]int)
	m["age"] = 20
	m["height"] = 180
	m["wight"] = 65
	m["sex"] = 1
	fmt.Println(m)
	fmt.Println(len(m))

	delete(m, "sex")
	fmt.Println(m)
	fmt.Println(m["age"])

	m1 := map[string]int{"one": 1, "two": 2, "three": 3}
	fmt.Println(m1)

	for k, v := range m1 {
		fmt.Printf("m1, %s, %d \r\n", k, v)
	}

	p := 100
	var pInt *int = &p
	fmt.Printf("p = %d, pInt=%p, *pInt=%d \n", p, pInt, *pInt)

	*pInt = 160
	fmt.Printf("p = %d, pInt=%p, *pInt=%d \n", p, pInt, *pInt)

	p = 200
	fmt.Printf("p = %d, pInt=%p, *pInt=%d \n", p, pInt, *pInt)
}
