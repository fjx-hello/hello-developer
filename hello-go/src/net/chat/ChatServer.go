package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"time"
)

// 消息
type Msg struct {
	sender  Client
	content string
}

func (msg *Msg) buildContent() string {
	sender := msg.sender
	return fmt.Sprintf("[%s - %s]说：%s", sender.addr, sender.name, msg.content)
}

// 客户端
type Client struct {
	ch        chan Msg
	conn      net.Conn
	cid       string
	name      string
	addr      string
	loginTime time.Time
}

func (client *Client) isLogin() bool {
	return client.name != ""
}

func (client *Client) login(name string) {
	client.name = name
}

func (client *Client) toString() string {
	return fmt.Sprintf("[%s - %s - %s]", client.cid, client.addr, client.name)
}

// 监听消息
func (client *Client) listen() {
	go func(ch chan Msg) {
		for {
			if msg, ok := <-ch; ok {
				// 自己发的话不显示了
				if msg.sender.cid != client.cid {
					client.send(msg.buildContent())
				}
			} else {
				fmt.Println(client.toString(), "closed")
				break
			}
		}
	}(client.ch)

	go func() {
		for {
			buf := make([]byte, 2048)
			n, err := client.conn.Read(buf)
			if n == 0 {
				client.close()
				broadcast(system, fmt.Sprintf("%s exit", client.name))
				break
			}

			if err != nil {
				fmt.Println(client.toString(), "read error", err)
				continue
			}
			// 去掉换行（nc会多个换行）
			text := string(buf[:n-1])
			if strings.HasPrefix(text, "login:") {
				client.login(string([]byte(text)[6:]))
				broadcast(system, fmt.Sprintf("welcome: %s - %s", client.addr, client.name))
				continue
			}
			if !client.isLogin() {
				client.send("please input your name to login")
				continue
			}
			// 忽略空消息
			if text == "" {
				continue
			}
			switch text {
			case "list:":
				client.send(listClients())
			default:
				broadcast(*client, text)
			}

		}
	}()

}

func (client *Client) send(content string) {
	_, err := io.WriteString(client.conn, content+"\n")
	if err != nil {
		fmt.Println(client.toString(), "send msg error", err)
	}
}

func (client *Client) close() {
	delete(clients, client.cid)
	close(client.ch)
	err := client.conn.Close()
	if err != nil {
		fmt.Println("conn close error", client.conn, err)
	}
}

// 完成登录的客户端
var clients = make(map[string]*Client, 5)

// 广播消息
var broadcastMsgCh = make(chan Msg)

// 系统
var system = Client{cid: "0", name: "system", addr: "server"}

// 处理客户端连接
func handle(conn net.Conn) {
	addr := conn.RemoteAddr().String()
	cid := fmt.Sprintf("%x", md5.New().Sum([]byte(addr)))
	client := Client{conn: conn, addr: addr, loginTime: time.Now(), cid: cid, ch: make(chan Msg, 2)}
	clients[cid] = &client
	client.listen()
	client.send("welcome and please input your name to login")
	fmt.Println("client connect:", client.toString())
}

// 广播
func broadcast(sender Client, content string) {
	msg := Msg{sender: sender, content: content}
	broadcastMsgCh <- msg
}

// 监听广播消息
func onMsg() {
	for {
		if msg, ok := <-broadcastMsgCh; ok {
			for _, client := range clients {
				go func(ch chan Msg) {
					ch <- msg
				}(client.ch)
			}
		}

	}
}

// 列出在线客户端
func listClients() string {
	list := make([]string, len(clients))
	for _, value := range clients {
		list = append(list, fmt.Sprintf("[%s - %s]", value.addr, value.name))
	}
	return strings.Join(list, "\n")
}

func main() {
	port := ":8888"

	listener, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal("server start error", err)
	}

	defer func() {
		cErr := listener.Close()
		if cErr != nil {
			fmt.Println("listener close error", cErr)
		}
	}()

	fmt.Println("server started", port)
	go onMsg()
	for {
		conn, aErr := listener.Accept()
		if aErr != nil {
			fmt.Println("accept error", aErr)
			continue
		}
		go handle(conn)
	}
}
