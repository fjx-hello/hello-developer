package clock

import (
	"flag"
	"io"
	"log"
	"net"
	"time"
)

// 简单的时钟服务
func main() {
	port := flag.String("port", "8080", "server port")
	flag.Parse()
	listener, err := net.Listen("tcp", "localhost:"+*port)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("server started", *port)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("Accept conn error", err)
			continue
		}
		go handleConn(conn)
	}

}

func handleConn(conn net.Conn) {
	defer conn.Close()

	for {
		_, err := io.WriteString(conn, time.Now().Format("15:04:05 \n"))
		if err != nil {
			log.Println(conn.RemoteAddr().String(), "close")
			return
		}
		time.Sleep(time.Second * 1)
	}

}
