package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {

	concurrencySum()
	concurrencySumWaitGroup()

}

func concurrencySum() {
	// 互斥锁
	var mut sync.Mutex
	count := 0
	for i := 0; i < 100; i++ {
		go func() {
			defer func() {
				mut.Unlock()
			}()
			mut.Lock()
			count++
		}()
	}

	time.Sleep(time.Millisecond * 50)

	fmt.Println("count", count)

}

// 类似java的CountDownLatch
func concurrencySumWaitGroup() {

	// 互斥锁
	var mut sync.Mutex
	var wait sync.WaitGroup
	count := 0
	for i := 0; i < 100; i++ {
		wait.Add(1)
		go func() {
			defer func() {
				mut.Unlock()
			}()
			mut.Lock()
			count++
			wait.Done()
		}()
	}

	wait.Wait()
	fmt.Println("count", count)

}
