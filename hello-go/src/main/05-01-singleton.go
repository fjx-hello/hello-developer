package main

import (
	"fmt"
	"sync"
	"unsafe"
)

// 单利
func main() {

	for i := 0; i < 10; i++ {
		go func() {
			obj := getSingleton()
			// 打印指针地址
			fmt.Printf("%X \n", unsafe.Pointer(obj))
		}()
	}

	// 任意键退出
	fmt.Println(fmt.Scanln())
}

var s *Singleton
var once sync.Once

type Singleton struct {
}

// 获取对象单例
func getSingleton() *Singleton {
	once.Do(func() {
		fmt.Println("create Singleton")
		s = new(Singleton)
	})
	return s
}
