package main

import "fmt"

// 接口
func main() {

	var p Flyer
	p = new(Plane)
	p.fly()

	var b Flyer = &Bird{}
	b.fly()

	// 类型转换
	if v, ok := b.(Flyer); ok {
		if ok {
			fmt.Println(v, "是Flyer类型")
			v.fly()
		} else {
			fmt.Println(v, "不是Flyer类型")
		}
	}

}

type Flyer interface {
	fly()
}

type Plane struct {
}

func (p *Plane) fly() {
	fmt.Println("plan is fling")
}

type Bird struct {
}

func (b Bird) fly() {
	fmt.Println("bird is fling")
}
