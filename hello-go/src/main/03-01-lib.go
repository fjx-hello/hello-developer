package main

import (
	"fmt"
	cm "github.com/easierway/concurrent_map"
	"hello-go/src/lib"
)

func init() {
	fmt.Println("init.......")
}

func main() {

	// 使用第三方库
	m := cm.CreateConcurrentMap(99)
	m.Set(cm.StrKey("key"), 10)
	fmt.Println(m.Get(cm.StrKey("key")))

	lib.Hello("fengjx")

}
