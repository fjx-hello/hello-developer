package main

import (
	"fmt"
	"strings"
)

func main() {

	s1 := "hello"
	fmt.Println(s1, len(s1))

	s2 := "go语言"
	fmt.Println(s2, len(s2))

	// "语言"的unicode
	s3 := "\u8bed\u8a00"
	fmt.Println(s3, len(s3))

	// unicode编码转换
	r := []rune(s2)
	fmt.Printf("len=%d, r[0]=%x, r[3]=%x \n", len(r), r[0], r[3])

	// 遍历字符串，打印字符和unicode编码
	for _, v := range r {
		fmt.Printf("%[1]c, %[1]x \n", v)
	}

	// 字符串分割
	str := "a,b,c,f"
	parts := strings.Split(str, ",")
	for _, v := range parts {
		fmt.Println(v)
	}

	fmt.Println(strings.Join(parts, "-"))

	hello := "hellogo"
	fmt.Println(strings.Contains(hello, "hello"))

}
