package main

import (
	"fmt"
)

func main() {

	var arr1 [3]int
	fmt.Println(arr1)

	arr2 := [3]int{1, 3, 4}
	fmt.Println(arr2)

	fmt.Println(arr2[0])

	arr3 := [...]int{1, 2, 3, 45, 6, 9}
	fmt.Println(arr3)

	for i := 0; i < len(arr3); i++ {
		fmt.Printf("arr3[%d] = %d \n", i, arr3[i])
	}

	fmt.Println("==================")

	for idx, e := range arr3 {
		fmt.Printf("arr3[%d] = %d \n", idx, e)
	}

	fmt.Println("==================")

	for _, e := range arr3 {
		fmt.Println(e)
	}

	fmt.Println("==================")

	// 数组切片
	fmt.Println(arr3[1:2])
	fmt.Println(arr3[:2])
	fmt.Println(arr3[2:])
	fmt.Println(arr3[1:3])

	// 二维数组
	arr4 := [2][3]int{{1, 2, 3}, {4, 5, 6}}
	fmt.Println("arr4:", arr4)

	var arr5 [3][9]int
	for i := 0; i < 3; i++ {
		for j := 0; j < 9; j++ {
			arr5[i][j] = (i + 1) * j
		}
	}
	fmt.Println("arr5:", arr5)

	// 可变数组（切片-slice）
	var s1 []int
	fmt.Println("s1:", len(s1), cap(s1))
	for i := 0; i < 10; i++ {
		s1 = append(s1, i+2)
	}
	fmt.Println("s1:", len(s1), cap(s1))
	s2 := []int{1, 2, 3, 4, 5}
	fmt.Println("s2:", len(s2), cap(s2))

	// 初始化slice，指定长度和容量
	s3 := make([]int, 3, 5)
	fmt.Println("s3:", len(s3), cap(s3))
	s3 = append(s3, 10)
	fmt.Println("s3:", len(s3), cap(s3))

	// 扩容
	var s4 []int
	for i := 0; i < 1500; i++ {
		s4 = append(s4, i)
	}
	fmt.Println("s4:", len(s4), cap(s4))

}
