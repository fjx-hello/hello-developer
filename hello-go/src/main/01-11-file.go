package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// 文件操作
func main() {

	writeFile("test.txt")
	readFile("test.txt")

}

func writeFile(path string) {
	file, err := os.Create(path)
	if err != nil {
		log.Fatal("create file error")
	}

	defer file.Close()

	for i := 0; i < 100; i++ {
		n, wErr := file.WriteString("hello go \n")
		if wErr != nil {
			fmt.Println("write err:")
		}
		fmt.Println("write n=", n)
	}

}

func readFile(path string) {

	file, err := os.Open(path)
	if err != nil {
		log.Fatal("read file error")
	}

	buf, rErr := ioutil.ReadAll(file)
	if rErr != nil {
		log.Fatal("read content error")
	}
	fmt.Println(string(buf))

}
