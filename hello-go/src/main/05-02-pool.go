package main

import (
	"errors"
	"fmt"
	"time"
)

// 对象池
func main() {

	pool := createPool(10)

	// 尝试放置超出池大小的对象
	if err := pool.releaseObj(&Obj{}); err != nil {
		fmt.Println(err)
	}

	for i := 0; i < 20; i++ {
		go func() {
			if obj, err := pool.getObj(time.Second * 1); err == nil {
				time.Sleep(time.Millisecond * 900)
				fmt.Printf("get obj %T \n", obj)
				if ok := pool.releaseObj(obj); ok != nil {
					fmt.Println("releaseObj error", ok)
				}
			} else {
				fmt.Println("can not get obj", err)
			}
		}()
	}

	// 任意键退出
	fmt.Println(fmt.Scanln())
}

type Obj struct {
}

// 对象池
type ObjPool struct {
	poolCh chan *Obj
}

// 创建对象池
func createPool(size int) *ObjPool {
	pool := new(ObjPool)
	pool.poolCh = make(chan *Obj, size)
	for i := 0; i < size; i++ {
		pool.poolCh <- new(Obj)
	}
	return pool
}

// 获得对象
func (pool *ObjPool) getObj(timeout time.Duration) (*Obj, error) {
	select {
	case obj := <-pool.poolCh:
		return obj, nil
	case <-time.After(timeout):
		return nil, errors.New("time out")
	}
}

// 释放对象，放回对象池
func (pool *ObjPool) releaseObj(obj *Obj) error {
	select {
	case pool.poolCh <- obj:
		fmt.Println("releaseObj")
	default:
		return errors.New("releaseObj error")
	}
	return nil
}
