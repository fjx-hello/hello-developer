package main

import (
	"fmt"
	"runtime"
)

func main() {
	a := 10
	fmt.Println(a)
	if a%2 == 0 {
		fmt.Printf("%d 是偶数 \n", a)
	}

	// switch用法
	os := runtime.GOOS
	switch os {
	case "darwin":
		fmt.Println("is darwin")

	case "linux":
		fmt.Println("is linux")
	}

	// switch语句替代 if else
	for i := 0; i < 5; i++ {
		switch {
		case i == 0:
			fmt.Println("loop begin")
		case i%2 == 0:
			fmt.Println("even")
		default:
			fmt.Println("default")
		}
	}

	n := 0
	for n < 5 {
		n++
		fmt.Println("n =", n)
	}

	// 获得数字1-5中所有不重复的3位数
	for i := 1; i < 4; i++ {
		for j := 1; j < 4; j++ {
			for k := 1; k < 4; k++ {
				if i != j && j != k {
					fmt.Println(i*100 + j*10 + k)
				}
			}
		}
	}

	// 死循环
	flag := 0
	for {
		flag++
		if flag > 10 {
			fmt.Println("exit")
			break
		}
	}

}
