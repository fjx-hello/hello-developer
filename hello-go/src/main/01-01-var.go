package main

import "fmt"

// 连续常量赋值
const (
	Monday = iota + 1
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	Sunday
)

const (
	Read = 1 << iota
	Write
	Execute
)

const pi = 3.141592653

func main() {

	fmt.Println(Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday)
	fmt.Println(Read, Write, Execute)
	fmt.Println(pi)

	// 定义变量
	var a int = 1
	fmt.Println(a)

	// 自动类型推断
	var b = 2
	fmt.Println(b)

	c := 3
	fmt.Println(c)

	var d, e = 4, 5
	fmt.Println(d, e)

	var (
		f = 6
		g = 7
	)
	fmt.Println(f, g)
	// 交换变量值
	f, g = g, f
	fmt.Println(f, g)

}
