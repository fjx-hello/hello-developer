package main

import "fmt"

// 继承的实现方式（组合）
func main() {

	ext := new(HelloExt)
	ext.hi()
	ext.echo("abc")

}

type Hello struct {
}

func (h Hello) hi() {
	fmt.Println("hello")
}

func (h Hello) echo(data string) {
	fmt.Println("echo", data)
}

type HelloExt struct {
	Hello
}

func (he *HelloExt) echo(data string) {
	fmt.Println("echo ext", data)
}
