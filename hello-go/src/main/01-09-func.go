package main

import "fmt"

func main() {

	v1, v2 := returnMultValues()
	fmt.Println(v1, v2)

	fmt.Println(sum(1, 2, 3, 4, 5))

}

// 返回多个值
func returnMultValues() (int, int) {
	return 1, 2
}

// 可变参数
func sum(ops ...int) int {
	var res int
	for _, v := range ops {
		res += v
	}
	return res
}
