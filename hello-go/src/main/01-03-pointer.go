package main

import "fmt"

func main() {

	var a = 1
	var aPtr = &a
	fmt.Println(a, aPtr)
	fmt.Printf("%T, %T \n", a, aPtr)

}
