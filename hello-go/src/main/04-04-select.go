package main

import (
	"fmt"
	"strconv"
	"time"
)

func main() {

	ch1 := asyncDoSomething(1)
	ch2 := asyncDoSomething(2)

	select {
	case ret := <-ch1:
		fmt.Println(ret)
	case ret := <-ch2:
		fmt.Println(ret)
	case <-time.After(time.Second * 3):
		fmt.Println("time out")
	}

	fmt.Scanln()
	fmt.Println("end")
}

func asyncDoSomething(d int) chan string {
	ch := make(chan string, 1)
	go func() {
		fmt.Println(d, "async step 1")
		time.Sleep(time.Duration(d * 1000 * 1000 * 1000))
		fmt.Println(d, "async step 2")
		// (1)
		ch <- "async ret " + strconv.Itoa(d)
		fmt.Println(d, "async done")
	}()
	return ch
}
