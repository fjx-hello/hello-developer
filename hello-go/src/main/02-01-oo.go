package main

import "fmt"

// 面向对象
func main() {

	user1 := User{1, "fjx", 20, 1}
	fmt.Println(user1)

	user2 := User{Id: 2, UserName: "fengjx"}
	fmt.Println(user2)

	user3 := new(User)
	user3.Id = 3
	user3.UserName = "fengjianxin"
	fmt.Println(user3)

	fmt.Println(user3.login(3))

}

type User struct {
	Id       int32
	UserName string
	Age      int
	Sex      int
}

// 定义行为
func (u *User) login(id int32) bool {
	if u.Id == id {
		return true
	}
	return false
}
