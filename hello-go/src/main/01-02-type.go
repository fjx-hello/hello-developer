package main

import (
	"fmt"
	"math"
)

// bool
// string
// int  int8  int16  int32  int64
// uint uint8 uint16 uint32 uint64 uintptr
// byte // alias for uint8
// rune // alias for int32,represents a Unicode code point
// float32 float64
// complex64 complex128
func main() {

	var a int8 = 1
	fmt.Println(a)
	var b int64 = 2
	fmt.Println(b)
	fmt.Println(math.MaxInt8)
	fmt.Println(math.MaxInt16)
	fmt.Println(math.MaxInt32)
	fmt.Println(math.MaxInt64)
	fmt.Println(math.MaxFloat32)
	fmt.Println(math.MaxFloat64)

	var s string
	fmt.Println("s =", s)
	fmt.Println(len(s), s == "")

}
