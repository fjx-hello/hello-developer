package main

import (
	"fmt"
	"math/rand"
)

/**
 * 运算符
 */
func main() {
	fmt.Println("01-05-branch-loop.go")

	a := rand.Intn(10)
	fmt.Println(a)

	if a%2 == 0 {
		fmt.Printf("%d 是偶数", a)
	}

	if a%2 == 0 {
		fmt.Printf("%d 是偶数", a)
	} else {
		fmt.Printf("%d 是奇数", a)
	}

}
