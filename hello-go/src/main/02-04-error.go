package main

import (
	"errors"
	"fmt"
)

// 异常处理
func main() {

	ok, err := login("123")
	fmt.Println(ok, err)

	ok, err = login("abc")
	if err == LoginError {
		fmt.Println("login fail")
	} else {
		fmt.Println("login success")
	}

	somethingRecover()
	somethingPanic()

}

var LoginError = errors.New("password error")

func login(pwd string) (bool, error) {
	if "abc" == pwd {
		return true, nil
	}
	return false, LoginError
}

// 异常退出
func somethingPanic() {
	defer func() {
		fmt.Println("final")
	}()
	fmt.Println("do something")
	panic(errors.New("error"))
	// 强制退出，不输出调用栈，同时不会执行defer
	// os.Exit(-1)
}

// 异常捕捉
func somethingRecover() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("recover", err)
		} else {
			fmt.Println("final")
		}
	}()
	fmt.Println("do something")
	panic(errors.New("error"))
}
