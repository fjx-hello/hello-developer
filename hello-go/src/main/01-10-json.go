package main

import (
	"encoding/json"
	"fmt"
	"log"
)

func main() {
	obj := JsonObj{Id: 1, UserName: "fengjx", Age: 18}
	// 转存对象转json字符串
	str, err := json.Marshal(obj)
	if err != nil {
		log.Fatal("obj to json error", err)
	} else {
		fmt.Println(string(str))
	}

	jsonStr := `{"id":1,"userName":"fengjx","age":18}`
	var obj2 JsonObj
	// json字符串转对象
	err2 := json.Unmarshal([]byte(jsonStr), &obj2)
	if err2 != nil {
		log.Fatal("json to obj error", err2)
	}
	fmt.Println(obj2)

	map1 := make(map[string]interface{}, 3)
	map1["name"] = "fengjx"
	map1["height"] = 180
	json1, err1 := json.Marshal(map1)
	if err1 != nil {
		log.Fatal("map to json error")
	}
	fmt.Println(string(json1))

	map2 := make(map[string]interface{}, 2)
	err3 := json.Unmarshal([]byte(`{"height":180,"name":"fengjx"}`), &map2)
	if err3 != nil {
		log.Fatal("json to map error", err3)
	}
	fmt.Println(map2)
	// 忽略了错误
	height, _ := map2["height"].(int)
	fmt.Println("height:", height)

	// 忽略了错误
	name, _ := map2["name"].(string)
	fmt.Println("name:", name)

}

type JsonObj struct {
	Id       int    `json:"id"`
	UserName string `json:"userName"`
	Age      int    `json:"age"`
	Pwd      string `json:"-"`
}
