package main

import "fmt"

func main() {

	map1 := map[string]int{"a": 1, "b": 2, "c": 3}
	fmt.Println(map1, map1["a"], map1["b"])

	map2 := map[string]int{}
	map2["a"] = 10
	map2["b"] = 20
	fmt.Println("map2:", map2)

	// 初始化容量
	map3 := make(map[int]int, 10)
	fmt.Println("map3", map3, len(map3))

	// 判断值是否存在
	map4 := map[int]int{}
	fmt.Println(map4[1])
	v, exist := map4[1]
	fmt.Println(v, exist)
	map4[1] = 0
	fmt.Println(map4[1])
	v, exist = map4[1]
	fmt.Println(v, exist)

	if v, exist = map4[1]; exist {
		fmt.Println(v, "exist")
	} else {
		fmt.Println(v, "not exist")
	}

	// 字符串，不存在默认空字符串
	map5 := map[string]string{}
	fmt.Println(map5["a"])

	map6 := map[int]int{1: 10, 2: 20, 3: 30}
	for k, v := range map6 {
		fmt.Println(k, v)
	}

}
