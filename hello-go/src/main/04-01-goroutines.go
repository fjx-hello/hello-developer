package main

import (
	"fmt"
)

// 协程的使用
func main() {

	for i := 0; i < 10; i++ {
		go func(i int) {
			fmt.Println(i)
		}(i)
	}

	fmt.Scanln()
	fmt.Println("done")

}
