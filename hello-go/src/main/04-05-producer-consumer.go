package main

import "fmt"

// 使用channel实现生产者与消费者
func main() {
	ch := make(chan int, 3)
	dataProducer(ch, 10)
	dataConsumer(ch)

	// 任意键退出
	fmt.Println(fmt.Scanln())
}

func dataProducer(ch chan int, size int) {
	go func(c chan int) {
		for i := 0; i < size; i++ {
			fmt.Println("producer", i)
			c <- i
		}
		// 关闭channel，不再生产数据
		close(c)
	}(ch)
}

func dataConsumer(ch chan int) {
	go func(c chan int) {
		for {
			// 当channel关闭时，ok == false
			data, ok := <-c
			if ok {
				fmt.Println("consumer", data)
			} else {
				fmt.Println("data close")
				break
			}
		}
	}(ch)
}
