package main

import (
	"fmt"
	"time"
)

func main() {

	ch := asyncSomething()
	blockSomething()
	fmt.Println(<-ch)

	fmt.Scanln()
	fmt.Println("end")
}

func asyncSomething() chan string {
	// ch := make(chan string) // 这种方式(1)处会阻塞，直到chan输出(<-ch)
	ch := make(chan string, 1)
	go func() {
		fmt.Println("async step 1")
		time.Sleep(time.Millisecond * 3000)
		fmt.Println("async step 2")
		// (1)
		ch <- "async ret"
		fmt.Println("async done")
	}()
	return ch
}

func blockSomething() {
	fmt.Println("block step 1")
	time.Sleep(time.Millisecond * 2000)
	fmt.Println("block step 2")
	fmt.Println("block done")
}
