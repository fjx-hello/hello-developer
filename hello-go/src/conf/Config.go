package conf

import (
	"fmt"
	"github.com/olebedev/config"
	"io/ioutil"
	"log"
)

var cfg *config.Config

func init() {
	file, err := ioutil.ReadFile("/Users/fengjianxin/workspaces/git/gitlab/hello-developer/hello-go/src/conf/config.yml")
	if err != nil {
		log.Fatal("load config error ", err)
	}
	yamlString := string(file)

	cfg, err = config.ParseYaml(yamlString)
	fmt.Println("config init")

}

func GetConfig() *config.Config {
	return cfg
}

func String(key string) string {
	val, err := cfg.String(key)
	if err != nil {
		log.Fatal("get config string error", err)
	}
	return val
}

func List(key string) []string {

	list, err := cfg.List(key)
	if err != nil {
		log.Fatal("get config list error", err)
	}
	res := make([]string, 3, 5)
	for _, c := range list {
		res = append(res, c.(string))
	}
	return res
}
