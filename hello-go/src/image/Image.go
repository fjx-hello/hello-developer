package main

import (
	"fmt"
	_ "image/jpeg"
	_ "image/gif"
	_ "image/png"
	_ "image/jpeg"
	"image"
	"log"
	"os"
)

func main() {
	file, err := os.Open("09200953.jpg")
	if err != nil {
		log.Fatal("open file", err)
	}
	image, name, err := image.Decode(file)

	if err != nil {
		log.Fatal("Decode", err)
	}
	fmt.Println(image.Bounds().String(), name, err)
}
