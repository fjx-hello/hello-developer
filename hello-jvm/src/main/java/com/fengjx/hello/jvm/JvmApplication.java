package com.fengjx.hello.jvm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fengjianxin
 */
@RestController
@SpringBootApplication
public class JvmApplication {

    public static void main(String[] args) {
        SpringApplication.run(JvmApplication.class, args);
    }



    @RequestMapping(value = "/jvm-big-obj")
    public String test1() {
        List<Byte[]> temp = new ArrayList<>();
        Byte[] b = new Byte[1024*1024*2];
        temp.add(b);

        return "success";
    }




}
