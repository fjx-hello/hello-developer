package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class TextBox implements Element {

    private String name;

    public TextBox(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print TextBox: " + name);
    }
}
