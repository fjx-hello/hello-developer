package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class Lable implements Element {

    private String name;

    public Lable(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print Lable: " + name);
    }
}
