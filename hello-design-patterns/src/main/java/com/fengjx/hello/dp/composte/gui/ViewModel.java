package com.fengjx.hello.dp.composte.gui;


import java.util.ArrayList;
import java.util.List;

/**
 * @author fengjianxin
 */
public class ViewModel implements View {

    private List<View> views = new ArrayList<>();

    public void addNode(View node) {
        views.add(node);
    }

    public void removeNode(View node) {
        views.remove(node);
    }

    @Override
    public void print() {
        for (View view : views) {
            view.print();
        }
    }
}
