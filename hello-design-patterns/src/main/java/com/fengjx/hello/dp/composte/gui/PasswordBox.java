package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class PasswordBox implements Element {

    private String name;

    public PasswordBox(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print PasswordBox: " + name);
    }
}
