package com.fengjx.hello.dp.cache;

import java.util.Properties;

/**
 * @author fengjianxin
 */
public interface CacheBuilder extends Cache {

    void reBuild(Properties conf);

}
