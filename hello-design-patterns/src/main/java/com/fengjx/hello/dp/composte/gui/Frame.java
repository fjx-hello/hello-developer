package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class Frame extends ViewModel {

    private String name;

    public Frame(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print Frame: " + name);
        super.print();
    }

}
