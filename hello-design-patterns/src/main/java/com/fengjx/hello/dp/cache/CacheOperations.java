package com.fengjx.hello.dp.cache;

/**
 * @author fengjianxin
 */
public interface CacheOperations extends Cache {

    Object get(Object key);

    void put(Object key, Object value);

    void delete(Object key);

}
