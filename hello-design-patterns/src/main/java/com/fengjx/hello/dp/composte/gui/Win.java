package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class Win extends ViewModel {

    private String name;

    public Win(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print Win: " + name);
        super.print();
    }
}
