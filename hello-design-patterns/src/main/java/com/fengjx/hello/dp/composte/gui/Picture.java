package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class Picture implements Element {

    private String img;

    public Picture(String img) {
        this.img = img;
    }

    @Override
    public void print() {
        System.out.println("print Picture: " + img);
    }
}
