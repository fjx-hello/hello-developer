package com.fengjx.hello.dp.cache;

import java.util.Properties;

/**
 * @author fengjianxin
 */
public class DefaultCache implements CacheOperations, CacheBuilder {

    @Override
    public Object get(Object key) {
        System.out.println("get cache");
        return null;
    }

    @Override
    public void put(Object key, Object value) {
        System.out.println("put cache");
    }

    @Override
    public void delete(Object key) {
        System.out.println("delete cache");
    }

    @Override
    public void reBuild(Properties conf) {
        System.out.println("rebuild cache");
    }

}
