package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class Button implements Element {

    private String name;

    public Button(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print Button: " + name);
    }
}
