package com.fengjx.hello.dp.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fengjianxin
 */
public class CacheFactory {

    private static final Map<String, Cache> CACHE_MAP;
    private static final String DEFAULT = "default";

    static {
        CACHE_MAP = new HashMap<>();
        CACHE_MAP.put(DEFAULT, new DefaultCache());
    }

    public CacheOperations getCache() {
        return (CacheOperations) CACHE_MAP.get(DEFAULT);
    }

    public CacheBuilder getCacheBuilder() {
        return (CacheBuilder) CACHE_MAP.get(DEFAULT);
    }

}
