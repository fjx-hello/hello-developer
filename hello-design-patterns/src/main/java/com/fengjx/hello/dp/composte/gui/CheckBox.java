package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class CheckBox implements Element {

    private String name;

    public CheckBox(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print CheckBox: " + name);
    }
}
