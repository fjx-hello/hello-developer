package com.fengjx.hello.dp.composte.gui;

/**
 * @author fengjianxin
 */
public class LinkLable implements Element {

    private String name;

    public LinkLable(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println("print LinkLable: " + name);
    }
}
