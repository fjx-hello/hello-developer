package com.fengjx.hello.dp.composte.gui;


import java.awt.*;

public class GuiClient {

    public static void main(String[] args) {
        ViewModel view = new Win("主窗口");

        Picture pic = new Picture("LOGO图片");
        view.addNode(pic);

        Button loginBtn = new Button("登录");
        view.addNode(loginBtn);
        Button loginReg = new Button("注册");
        view.addNode(loginReg);

        ViewModel frame1 = new Frame("FRAME1");
        Lable labUserName = new Lable("用户名");
        frame1.addNode(labUserName);
        TextBox txtUserName = new TextBox("用户名文本框");
        frame1.addNode(txtUserName);
        Lable labPwd = new Lable("密码");
        frame1.addNode(labPwd);
        TextBox pwdBox = new TextBox("密码框");
        frame1.addNode(pwdBox);
        CheckBox remember = new CheckBox("记住用户名");
        frame1.addNode(remember);
        LinkLable forget = new LinkLable("忘记密码");
        frame1.addNode(forget);
        view.addNode(frame1);

        view.print();
    }

}
