package com.fengjx.hello.dp.cache;


import org.junit.Test;

public class CacheTest {

    private static final CacheFactory FACTORY = new CacheFactory();

    @Test
    public void testOperations() {
        CacheOperations opt = FACTORY.getCache();
        opt.put("k1", "v1");
        opt.get("k1");
        opt.delete("k1");
    }

    @Test
    public void testRebuild() {
        CacheBuilder cacheBuilder = FACTORY.getCacheBuilder();
        cacheBuilder.reBuild(null);
    }

}
