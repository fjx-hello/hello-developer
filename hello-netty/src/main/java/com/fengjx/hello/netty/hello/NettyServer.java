package com.fengjx.hello.netty.hello;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.ReferenceCountUtil;

public class NettyServer {

    private final int port;

    public NettyServer(int port) {
        this.port = port;
    }

    public void start() throws Exception {
        EventLoopGroup accept = new NioEventLoopGroup(5);
        EventLoopGroup worker = new NioEventLoopGroup(20);
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(accept, worker).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel sc) {
                            sc.pipeline().addLast(new StringDecoder());
                            sc.pipeline().addLast(new StringEncoder());
                            sc.pipeline().addLast(new EchoServerHandler());
                        }
                    });
            b.option(ChannelOption.SO_BACKLOG, 1024);
            // 发送数据缓冲区大小
            b.option(ChannelOption.SO_SNDBUF, 16 * 1024);
            // 接收数据缓冲区大小
            b.option(ChannelOption.SO_RCVBUF, 16 * 1024);
            b.childOption(ChannelOption.SO_KEEPALIVE, true);
            System.out.println("server start , port : " + port);
            ChannelFuture f = b.bind(port).sync();
            f.channel().closeFuture().sync();
        } finally {
            accept.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        new NettyServer(6666).start();
    }

    static class EchoServerHandler extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) {
            try {
                String data = (String) msg;
                if (data == null || data.trim().length() == 0) {
                    return;
                }
                System.out.println("Server received: " + data);
                ctx.writeAndFlush(Unpooled.copiedBuffer(("received: " + data).getBytes()));
            } finally {
                ReferenceCountUtil.release(msg);
            }

        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
            cause.printStackTrace();
            ctx.close();
        }
    }
}
